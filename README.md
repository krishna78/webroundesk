# Roundesk

Video conferencing app.

## Installation

**Note:** `remote.roundesk.io` should be changed with your domain!

Make sure you have Ubuntu 18.04. Run the following command to check ubuntu version.

    lsb_release -a

You should login to root user to avoid permission issues. Run the following command to switch to root user if you are on normal user.

    sudo su -

We will be using `vi` for editing files on server. Refer the following commands to handle vi.

- open a file - `vi file_path_name`
- switch to edit mode - press "I"
- turn off edit mode for save - press "ESC"
- save file - press ":x" and "Enter"

### Step 1: Install build tools

    sudo apt-get update -y
    sudo apt-get install software-properties-common -y
    sudo apt-add-repository universe
    export LC_ALL="C"
    sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates unzip gcc g++ make gnupg2 git certbot

### Step 2: Install NodeJS

    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    sudo apt -y install nodejs

### Step 3: Add host name
    sudo hostnamectl set-hostname meet
    vi /etc/hosts # edit host file

    # Press "I"
    # add something similar to the following (add public IP to domain mapping)
    # 127.0.0.1 localhost
    x.x.x.x remote.roundesk.io meet
    # Press "esc"
    # Enter :x (save and exit)

    ping "$(hostname)"

### Step 4: Open Ports with Firewall (You need to enable inbound and outbound on your cloud interface as well)

    sudo ufw allow 80/tcp
    sudo ufw allow 443/tcp
    sudo ufw allow 4443/tcp
    sudo ufw allow 10000/udp
    sudo ufw allow 22/tcp
    sudo ufw allow 5281/tcp
    sudo ufw allow 10000:65535/udp
    sudo ufw allow 5222/tcp
    sudo ufw allow 5269/tcp
    sudo ufw allow 5347/tcp
    sudo ufw enable
    sudo ufw status verbose

### Step 5: Install Jitsi

    curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
    echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
    sudo apt update
    sudo apt install jitsi-meet -y
    # Enter host name when asks remote.roundesk.io
    # Select certificate option as "Generate a new self-signed certificate"

### Step 6: Check and fix Nginx

    nginx -t && nginx -s reload

Follow `step 7`, if there is an error from `step 6`

### Step 7 (Optional): Fix nginx error

    vi /etc/nginx/sites-enabled/remote.roundesk.io.conf
    # Remove "TLSv1.3" from ssl_protocols
    # Save and close
    nginx -t && nginx -s reload

### Step 8: Install Certificate

By now, your domain should be accessible for public people to proceed with this step. Check your domain is working or not.

    sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
    # Enter an email id when asks

### Step 9: Test the URL for jitsi installation

Navigate to your domain and test all base jitsi functionalities.

### Step 10 (Optional): Update config if you are setting up new domain (do this step from your development machine)

- Checkout latest code from `development` branch - `git checkout development` and `git checkout -b my-new-domain`
- Find and replace `remote.roundesk.io` to your domain on `config.js`, `webpack.config.js`, `react/features/base/settings/constants.js`
- Update API URL - update `API_ENDPOINT` on `interface_config.js`, `DEFAULT_API_URL` on `react/features/base/settings/constants.js`
- Add the changes to your branch - `git add .`
- Commit and push changes - `git commit -m "Add config for my new domain"` then `git push origin my-new-domain`
- All the JITSI configurations can be managed using `config.js` and `interface_config.js`

### Step 11: Add Roundesk Code

    rm -rf /usr/share/jitsi-meet
    cd /usr/share/
    # get git credentials
    sudo git clone https://gitlab.com/evvoiot/roundesk/video/meet-roundesk.io jitsi-meet
    cd jitsi-meet
    # choose your branch (`uat` for uat, `development` for remote)
    git checkout development
    sudo npm install
    make

### Step 12: Update systemd limits to support more participants

    # Edit and add the following 3 lines
    vi /etc/systemd/system.conf

    DefaultLimitNOFILE=65000
    DefaultLimitNPROC=65000
    DefaultTasksMax=65000

    sudo systemctl daemon-reload
    sudo systemctl restart jitsi-videobridge2
    sudo systemctl status jitsi-videobridge2

### Step 13: Configure Roundesk

    cp /etc/jitsi/meet/remote.roundesk.io-config.js /etc/jitsi/meet/remote.roundesk.io-config.js.bak
    rm /etc/jitsi/meet/remote.roundesk.io-config.js
    cp /usr/share/jitsi-meet/config.js /etc/jitsi/meet/remote.roundesk.io-config.js

Edit prosody host file and add moderator module,

    vi /etc/prosody/conf.avail/remote.roundesk.io.cfg.lua

Under `Component "conference.remote.roundesk.io" "muc"` component `modules_enabled` add `"muc_moderators";`. It should look like,

    Component "conference.remote.roundesk.io" "muc"
        storage = "memory"
        modules_enabled = {
            "muc_meeting_id";
            "muc_domain_mapper";
	        "muc_moderators";
        }
        admins = { "focus@auth.remote.roundesk.io" }
        muc_room_locking = false
        muc_room_default_public_jids = true

### Step 14: Remove logs

    rm /var/log/jitsi/jicofo.log
    rm /var/log/jitsi/jvb.log
    rm /var/log/prosody/*
    rm /var/log/nginx/*

### Step 15: Restart all services

    service jitsi-videobridge2 restart
    service prosody restart
    service jicofo restart
    service nginx restart

### Step 16: Test all features

Navigate to your domain and test all roundesk functionalities.
### Step 17 (Optional): Update Prosody if you need meeting timer

    wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb
    sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb
    echo 'deb https://packages.prosody.im/debian focal main' | sudo tee /etc/apt/sources.list.d/prosody.list
    wget https://prosody.im/files/prosody-debian-packages.key -O- | sudo apt-key add -
    sudo apt update
    apt --fix-broken install
    sudo apt upgrade prosody -y
    systemctl status prosody
    sudo systemctl start prosody
    sudo systemctl enable prosody
    sudo netstat -lnptu | grep lua

    prosodyctl about
    sudo prosodyctl check config

Update prosody settings

    vi /etc/prosody/prosody.cfg.lua

    # add the following
    Include "conf.d/*.cfg.lua"

Configure Video Bridge (Make sure you have `--apis=xmpp,rest` for `JVB_OPTS`)

    vi /etc/jitsi/videobridge/config

    # extra options to pass to the JVB daemon
    JVB_OPTS="--apis=xmpp,rest"

Update prosody host (`/etc/prosody/conf.avail/remote.roundesk.io.cfg.lua`)

Enable CORS by changing `cross_domain_bosh = false;` to `cross_domain_bosh = true;`

Add `muc_room_cache_size = 1000` to `Component "internal.auth.remote.roundesk.io" "muc"`

Set JVB password on prosody (enter the same password twice which is copied from `/etc/jitsi/videobridge/config`)

    prosodyctl passwd jvb@auth.remote.roundesk.io

    sudo chown prosody:prosody -R /var/lib/prosody/
    sudo chown prosody:prosody -R /etc/prosody/
    sudo service prosody restart

*Important*: Update storage from "none" to "memory" on host file `/etc/prosody/conf.avail/remote.roundesk.io.cfg.lua` (2-3 places will be there to update)

Check for certificate issues:

    prosodyctl check

Generate certificate if there is an issue from above command.

    prosodyctl cert generate remote.roundesk.io auth.remote.roundesk.io

### Step 18 (Optional): only if your server is behind NAT

    vi /etc/jitsi/videobridge/sip-communicator.properties
    # Remove org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES

    # Add the following
    org.jitsi.videobridge.NAT_HARVESTER_LOCAL_ADDRESS=<Private IP>
    org.jitsi.videobridge.NAT_HARVESTER_PUBLIC_ADDRESS=<Public IP>
    org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=<Private IP>
    org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<Public IP>

### Step 19 (Optional): Install Video Server

On front-end server open the file `/etc/jitsi/videobridge/sip-communicator.properties` It should look similar to this.

    org.ice4j.ice.harvest.DISABLE_AWS_HARVESTER=true
    org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES=meet-jit-si-turnrelay.jitsi.net:443
    org.jitsi.videobridge.ENABLE_STATISTICS=true
    org.jitsi.videobridge.STATISTICS_TRANSPORT=muc
    org.jitsi.videobridge.xmpp.user.shard.HOSTNAME=localhost
    org.jitsi.videobridge.xmpp.user.shard.DOMAIN=auth.meet.example.com
    org.jitsi.videobridge.xmpp.user.shard.USERNAME=jvb
    org.jitsi.videobridge.xmpp.user.shard.PASSWORD=7sM1g8yw
    org.jitsi.videobridge.xmpp.user.shard.MUC_JIDS=JvbBrewery@internal.auth.meet.example.com
    org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME=728f25ec-a170-40f5-a7f1-b4f7b9d8c98a

Please add the following lines to the end of this file.

    org.jitsi.videobridge.DISABLE_TCP_HARVESTER=true
    org.jitsi.videobridge.xmpp.user.shard.DISABLE_CERTIFICATE_VERIFICATION=true

In addition, please change the line `org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME` to this.

    org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME=jvb1

Configure the second machine (video bridge server)

    echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list
    wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | apt-key add -
    apt update
    apt install jitsi-videobridge2 -y

Again, you will be asked for the hostname of your installation. Please enter the FQDN of the first machine (in our case remote.roundesk.io)

Next, also open the file `/etc/jitsi/videobridge/sip-communicator.properties` and add the following lines to the end.

    org.jitsi.videobridge.DISABLE_TCP_HARVESTER=true
    org.jitsi.videobridge.xmpp.user.shard.DISABLE_CERTIFICATE_VERIFICATION=true

Find the line `org.jitsi.videobridge.xmpp.user.shard.HOSTNAME` and set it to the public ip address of the first machine.

    - org.jitsi.videobridge.xmpp.user.shard.HOSTNAME=localhost
    + org.jitsi.videobridge.xmpp.user.shard.HOSTNAME=<public ip address>

In addition, please change the line `org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME` to this.

    org.jitsi.videobridge.xmpp.user.shard.MUC_NICKNAME=jvb2

Get the credentials

On the first machine, please open the file `/etc/jitsi/videobridge/config` and find the line `JVB_SECRET`. Copy the password and go back to the second machine.

Replace the passwords in the following two files by the password you just copied.

    /etc/jitsi/videobridge/config
    /etc/jitsi/videobridge/sip-communicator.properties

### Step 20 (Optional): Timout config for nginx if you are facing bad gateway issues

Add timeout for nginx http-bind location settings (`/etc/nginx/sites-available/remote.roundesk.io.conf`)

    proxy_read_timeout 3600;

### Step 21: Adding moderators concept (allow roundesk host to kick-out participants)

Run `patch /usr/lib/prosody/modules/muc/muc.lib.lua < /usr/share/jitsi-meet/resources/prosody-plugins/muc_owner_allow_kick.patch`

Add `"muc_moderators";` inside `modules_enabled` of `Component "conference.uat.roundesk.io" "muc"` under prosody host config file.

### Step 22: Remove logs

    rm /var/log/jitsi/jicofo.log
    rm /var/log/jitsi/jvb.log
    rm /var/log/prosody/*
    rm /var/log/nginx/*

### Step 23: Restart all services

    service jitsi-videobridge2 restart
    service prosody restart
    service jicofo restart
    service nginx restart
### Step 24: Building mobile app for deployment

TBD
### Config Files

- Roundesk Config File `/etc/jitsi/meet/remote.roundesk.io-config.js`
- Prosody Main Config `/etc/prosody/prosody.cfg.lua`
- Prosody Host Config `/etc/prosody/conf.avail/remote.roundesk.io.cfg.lua`
- Nginx Host Config `/etc/nginx/sites-enabled/remote.roundesk.io.conf`
- Jicofo Config `/etc/jitsi/jicofo/sip-communicator.properties`
- Videobridge Config `/etc/jitsi/videobridge/sip-communicator.properties` and `/etc/jitsi/videobridge/config`

### Log Files

- Prosody Error `/var/log/prosody/prosody.err`
- Prosody Log `/var/log/prosody/prosody.log`
- Jicofo `/var/log/jitsi/jicofo.log`
- Video Bridge `/var/log/jitsi/jvb.log`
- Nginx Log `/var/log/nginx/access.log`
- Nginx Error `/var/log/nginx/error.log`



### Note - how to check jitsi version?

    dpkg -l | grep jitsi


sudo apt-get install nginx-extras
Go to /etc/nginx/nginx.conf and under http add:

http {
    more_set_headers "Server: Your_New_Server_Name";
    server_tokens off;
}
sudo service nginx restart