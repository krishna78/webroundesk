/* eslint-disable no-unused-vars, no-var */
var config = {
    hosts: {
        domain: 'uat.roundesk.io',
        focus: 'focus.uat.roundesk.io',
        muc: 'conference.uat.roundesk.io'
    },
    bosh: '//uat.roundesk.io/http-bind',
    // constraints: {
    //     video: {
    //         height: {
    //             ideal: 720,
    //             max: 720,
    //             min: 180
    //         },
    //         width: {
    //             ideal: 1280,
    //             max: 1280,
    //             min: 320
    //         }
    //     }
    // },

    // Websocket URL
    // websocket: 'wss://uat.roundesk.io/xmpp-websocket',
    clientNode: 'http://jitsi.org/jitsimeet',
    testing: {
        callStatsThreshold: 1, // % of users that will have callStats enabled.
        capScreenshareBitrate: 1,
        octo: {
            probability: 1
        }
    },
    disableAudioLevels: true,
    enableNoAudioDetection: false,
    enableNoisyMicDetection: false,
    startAudioOnly: true,
    startAudioMuted: 9,
    startWithAudioMuted: true,
    // resolution: 360, // 720,
    resolution: 480,
    constraints: {
        video: {
            aspectRatio: 16 / 9,
            height: {
                ideal: 480,
                max: 480,
                min: 480
            }
        }
    },

    // maxFullResolutionParticipants: 2,
    disableSimulcast: false,
    enableLayerSuspension: true,
    startVideoMuted: 9,
    startWithVideoMuted: true,

    // Try to start calls with screen-sharing instead of camera video.
    // startScreenSharing: false,
    liveStreamingEnabled: true,
    transcribingEnabled: false,
    channelLastN: -1,
    videoQuality: {
        maxBitratesVideo: {
            low: 200000,
            standard: 500000,
            high: 1500000
        }
    },
    enableTcc: true,
    enableRemb: true,
    useTurnUdp: false,
    // openBridgeChannel: 'websocket',
    requireDisplayName: true,
    enableWelcomePage: true,
    enableUserRolesBasedOnToken: false,
    prejoinPageEnabled: true,
    enableP2P: true, // flag to control P2P connections
    p2p: {
        enabled: true,
        disableH264: true,
        useStunTurn: true
    },
    useStunTurn: true,
    analytics: {
        googleAnalyticsTrackingId: 'G-250613968',
        scriptURLs: ['libs/analytics-ga.min.js']
    },
    apiLogLevels: ['warn', 'error'],

    // chromeExtensionBanner: {
    //     // The chrome extension to be installed address
    //     url: 'https://chrome.google.com/webstore/detail/jitsi-meetings/kglhbbefdnlheedjiejgomgmfplipfeb',

    //     // Extensions info which allows checking if they are installed or not
    //     chromeExtensionsInfo: [
    //         {
    //             id: 'kglhbbefdnlheedjiejgomgmfplipfeb',
    //             path: 'jitsi-logo-48x48.png'
    //         }
    //     ]
    // },

    e2eping: {
        pingInterval: -1
    },
    disableLocalVideoFlip: false,
    makeJsonParserHappy: 'even if last key had a trailing comma',
    disableAP: false,
    disableAEC: false,
    disableNS: false,
    disableAGC: false,
    disableHPF: false,
    stereo: true,
    enableLipSync: false,
    disableRtx: false, // Enables RTX everywhere
    enableScreenshotCapture: false,
    startBitrate: '800',
    disableSuspendVideo: true,
    forceJVB121Ratio: -1,
    enableTalkWhileMuted: false,
    defaultLanguage: 'en'
};

/* eslint-enable no-unused-vars, no-var */
