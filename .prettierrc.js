module.exports = {
    arrowParens: 'avoid',
    bracketSpacing: false,
    jsxBracketSameLine: true,
    singleQuote: true,
    trailingComma: 'none',
    jsxSingleQuote: true,
    useTabs: false,
    tabWidth: 4,
    parser: 'typescript',
    printWidth: 100,
    overrides: [
        {
            files: '*.scss',
            options: {
                parser: 'css'
            }
        }
    ],
    eslintIntegration: true
};
