// @flow

import React from 'react';
import { toArray } from 'react-emoji-render';


import { translate } from '../../../base/i18n';
import { Linkify } from '../../../base/react';
import { MESSAGE_TYPE_LOCAL } from '../../constants';
import AbstractChatMessage, {
    type Props
} from '../AbstractChatMessage';
import PrivateMessageButton from '../PrivateMessageButton';

/**
 * Renders a single chat message.
 */
class ChatMessage extends AbstractChatMessage<Props> {
    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { message } = this.props;
        const processedMessage = [];

        // content is an array of text and emoji components
        const content = toArray(this._getMessageText(), { className: 'smiley' });

        content.forEach(i => {
            if (typeof i === 'string') {
                processedMessage.push(<Linkify key={ i }>{ i }</Linkify>);
            } else {
                processedMessage.push(i);
            }
        });

        return (
            // <>
            //     <li className='sent'>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <label className='mb-0 mr-3'>
            //             Balaji koushikan
            //                                     </label>
            //         <p className='mb-0'>
            //             How the hell am I supposed to get a jury to
            //             believe you when I am not even sure that I do?!
            //                                     </p>
            //     </li>
            //     <li className='replies'>
            //         <label className='mb-0 mr-3'>
            //             priyadarishini.j
            //                                     </label>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <p>
            //             When you're backed against the wall, break the
            //             god damn thing down.
            //                                     </p>
            //     </li>
            //     <li className='replies'>
            //         <label className='mb-0 mr-3'>
            //             priyadarishini.j
            //                                     </label>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <p>Excuses don't win championships.</p>
            //     </li>
            //     <li className='sent'>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <label className='mb-0 mr-3'>
            //             Balaji koushikan
            //                                     </label>
            //         <p>Oh yeah, did Michael Jordan tell you that?</p>
            //     </li>
            //     <li className='replies'>
            //         <label className='mb-0 mr-3'>
            //             priyadarishini.j
            //                                     </label>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <p>No, I told him that.</p>
            //     </li>
            //     <li className='replies'>
            //         <label className='mb-0 mr-3'>
            //             priyadarishini.j
            //                                     </label>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <p>
            //             What are your choices when someone puts a gun to
            //             your head?
            //                                     </p>
            //     </li>
            //     <li className='sent'>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <label className='mb-0 mr-3'>
            //             Balaji koushikan
            //                                     </label>
            //         <p>
            //             What are you talking about? You do what they say
            //             or they shoot you.
            //                                     </p>
            //     </li>
            //     <li className='replies'>
            //         <label className='mb-0 mr-3'>
            //             priyadarishini.j
            //                                     </label>
            //         <label className='mt-0'>11:01 AM | June 9</label>
            //         <p>
            //             Wrong. You take the gun, or you pull out a
            //             bigger one. Or, you call their bluff. Or, you do
            //             any one of a hundred and forty six other things.
            //                                     </p>
            //     </li>
            // </>
            <>
                <li className={ message.messageType === MESSAGE_TYPE_LOCAL ? 'sent' : 'replies' }>

                    <div className='message-info'>
                        <label className='mb-0 mr-3'>
                            { this.props.showDisplayName && this._renderDisplayName() }

                        </label>
                        <label className='mt-0 mr-2'>{ this.props.showTimestamp && this._renderTimestamp() }</label>
                    </div>
                    <p className='mb-0'>
                        { processedMessage }
                        { message.privateMessage && this._renderPrivateNotice() }
                    </p>
                    <div>
                        { message.privateMessage && message.messageType !== MESSAGE_TYPE_LOCAL
                            && (
                                <div className='messageactions'>
                                    <PrivateMessageButton
                                        participantID={ message.id }
                                        reply={ true }
                                        showLabel={ false } />
                                </div>
                            ) }
                    </div>
                </li>
            </>
            // <div className='chatmessage-wrapper'>

            //     <div className={ `chatmessage ${message.privateMessage ? 'privatemessage' : ''}` }>

            //         <div className='replywrapper'>

            //             <div className='messagecontent'>

            //                 <div className='usermessage'>
            //                     { processedMessage }
            //                 </div>
            //                 { message.privateMessage && this._renderPrivateNotice() }
            //             </div>
            //             { message.privateMessage && message.messageType !== MESSAGE_TYPE_LOCAL
            //                 && (
            //                     <div className='messageactions'>
            //                         <PrivateMessageButton
            //                             participantID={ message.id }
            //                             reply={ true }
            //                             showLabel={ false } />
            //                     </div>
            //                 ) }
            //         </div>
            //     </div>

            // </div>
        );
    }

    _getFormattedTimestamp: () => string;

    _getMessageText: () => string;

    _getPrivateNoticeMessage: () => string;

    /**
     * Renders the display name of the sender.
     *
     * @returns {React$Element<*>}
     */
    _renderDisplayName() {
        return (
            <>
                { this.props.message.displayName }
            </>
        );
    }

    /**
     * Renders the message privacy notice.
     *
     * @returns {React$Element<*>}
     */
    _renderPrivateNotice() {
        return (
            <span className='privatemessagenotice'>
                { this._getPrivateNoticeMessage() }
            </span>
        );
    }

    /**
     * Renders the time at which the message was sent.
     *
     * @returns {React$Element<*>}
     */
    _renderTimestamp() {
        return (
            <>
                { this._getFormattedTimestamp() }
            </>
        );
    }
}

export default translate(ChatMessage);
