//@flow

import React, {Component} from 'react';
import type {Dispatch} from 'redux';
import {Dialog} from '../../../base/dialog';
import {translate} from '../../../base/i18n';
import {connect} from '../../../base/redux';
import {uploadFileToServer} from '../../actions.any';


type Props = {
    /**
     * Redux store dispatch function.
     */
    dispatch: Dispatch<any>;
}

type State = {

}

class UploadDialog extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.uploadFile = this.uploadFile.bind(this);
    }

    render() {
        // const {isModerator, t} = this.props;

        return (
            <Dialog cancelKey={'dialog.close'} className='rd-modal' submitDisabled={true}>
                <div className='audio-recorder'>
                    <p>
                        Uploaded file
                    </p>
                    <div className="upload-file-list">
                        {this.props.data.name}
                       {/* {this.props.data.type=='image/png' && <img src="this.props.data.name"></img>}  */}
                    </div>
                    <button
                        className='btn btn-danger mr-4'
                        type='button'
                        onClick={this.uploadFile}
                    >
                        Upload
                    </button>
                </div>
            </Dialog>
        );
    }


    uploadFile(){
        this.props.dispatch(uploadFileToServer(this.props.data));
    }
}

function _mapStateToProps(state) {
    const data = state['features/chat'].selectedFile;
    return{
        data
    }
}

function mapDispatchToProps (dispatch) {
    return{
        uploadFileToServer,
        dispatch
    }
    
};

export default translate(connect(_mapStateToProps,mapDispatchToProps)(UploadDialog));