// @flow
import axios from 'axios';
import {showNotification} from '../notifications/actions';
import {hideDialog} from '../base/dialog';
import {UploadDialog} from './components/web/UploadDialog';
import {NOTIFICATION_TYPE} from '../notifications/constants';
import {
    ADD_MESSAGE,
    CLEAR_MESSAGES,
    SEND_MESSAGE,
    SET_PRIVATE_MESSAGE_RECIPIENT,
    UPLOAD_FILE
} from './actionTypes';

/**
 * Adds a chat message to the collection of messages.
 *
 * @param {Object} messageDetails - The chat message to save.
 * @param {string} messageDetails.displayName - The displayName of the
 * participant that authored the message.
 * @param {boolean} messageDetails.hasRead - Whether or not to immediately mark
 * the message as read.
 * @param {string} messageDetails.message - The received message to display.
 * @param {string} messageDetails.messageType - The kind of message, such as
 * "error" or "local" or "remote".
 * @param {string} messageDetails.timestamp - A timestamp to display for when
 * the message was received.
 * @returns {{
 *     type: ADD_MESSAGE,
 *     displayName: string,
 *     hasRead: boolean,
 *     message: string,
 *     messageType: string,
 *     timestamp: string,
 * }}
 */
export function addMessage(messageDetails: Object) {
    return {
        type: ADD_MESSAGE,
        ...messageDetails
    };
}

/**
 * Clears the chat messages in Redux.
 *
 * @returns {{
 *     type: CLEAR_MESSAGES
 * }}
 */
export function clearMessages() {
    return {
        type: CLEAR_MESSAGES
    };
}

/**
 * Sends a chat message to everyone in the conference.
 *
 * @param {string} message - The chat message to send out.
 * @param {boolean} ignorePrivacy - True if the privacy notification should be ignored.
 * @returns {{
 *     type: SEND_MESSAGE,
 *     ignorePrivacy: boolean,
 *     message: string
 * }}
 */
export function sendMessage(message: string, ignorePrivacy: boolean = false) {
    return {
        type: SEND_MESSAGE,
        ignorePrivacy,
        message
    };
}

/**
 * Initiates the sending of a private message to the supplied participant.
 *
 * @param {Participant} participant - The participant to set the recipient to.
 * @returns {{
 *     participant: Participant,
 *     type: SET_PRIVATE_MESSAGE_RECIPIENT
 * }}
 */
export function setPrivateMessageRecipient(participant: Object) {
    return {
        participant,
        type: SET_PRIVATE_MESSAGE_RECIPIENT
    };
}

/**
 * Upload to a file to everyone in the conference.
 *
 * @param {Object} file - file to send out.
 * @returns {{
    *     type: UPLOAD_FILE,
    *     file: Object
    * }}
    */
   export function uploadFile(file: Object,url:String="") {
       return {
           type: UPLOAD_FILE,
           file,
           url
       };
   }


   /**
 * Get meeting infromation.
 *
 * @param {string} room  - Room name.
 * @param {*} token - Join token.
 * @param {*} cb - Navigation call back.
 * @returns {Function}.
 */
export function uploadFileToServer(file) {
    return (dispatch: Function, getState: Function) => {

        const data = new FormData();
        data.append('file',file);
        const config = { headers: { 'Content-Type': 'multipart/form-data' } };

        axios
            .post(`${interfaceConfig.API_ENDPOINT}/file-upload`, data, config)
            .then(res => {
                console.log('Response from Server:');
                console.log(JSON.stringify(res.data));
                if(res.data.success){
                    dispatch(uploadFile(file,res.data.url));
                    dispatch(hideDialog(UploadDialog));
                    dispatch(sendMessage(res.data.url));
                }
                dispatch(
                    showNotification(
                        {
                            title: res.data.message,
                            appearance: NOTIFICATION_TYPE.SUCCESS
                        },
                        50000
                    )
                );
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;
                    dispatch(hideDialog(UploadDialog));
                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }
                console.log(err);

                dispatch(
                    showNotification(
                        {
                            title: "Upload file failed",
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        50000
                    )
                );
            });
    };
}
