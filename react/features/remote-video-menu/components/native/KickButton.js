// @flow

import { translate } from '../../../base/i18n';
import { isLocalParticipantModerator } from '../../../base/participants';
import { connect } from '../../../base/redux';
import AbstractKickButton from '../AbstractKickButton';

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @returns {Props}
 */
function _mapStateToProps(state) {
    const meeting = state['features/welcome'].meeting;

    return {
        visible: meeting && meeting.settings && meeting.settings.kickOutParticipant// isLocalParticipantModerator(state)
    };
}

export default translate(connect(_mapStateToProps)(AbstractKickButton));
