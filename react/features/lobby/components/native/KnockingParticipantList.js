// @flow

import React from 'react';
import { ScrollView, Text, View, TouchableOpacity } from 'react-native';

import { Avatar } from '../../../base/avatar';
import { translate } from '../../../base/i18n';
import { connect } from '../../../base/redux';
import AbstractKnockingParticipantList, {
    mapStateToProps as abstractMapStateToProps,
    type Props
} from '../AbstractKnockingParticipantList';
import { admitParticipant, rejectParticipant } from '../../../toolbox/actions.native';

import styles from './styles';


/**
 * Component to render a list for the actively knocking participants.
 */
class KnockingParticipantList extends AbstractKnockingParticipantList {

    /**
     * Implements {@code PureComponent#render}.
     *
     * @inheritdoc
     */
    render() {
        const { _participants, _visible, t, waitingParticipantsList, isHost } = this.props;

        if (!isHost) {
            return null;
        }



        return (
            <ScrollView
                style={ styles.knockingParticipantList }>
                { waitingParticipantsList.map(p => (
                    <View
                        key={ p.code }
                        style={ styles.knockingParticipantListEntry }>
                        {/* <Avatar
                            displayName={ p.name }
                            size={ 48 }
                            url={ p.loadableAvatarUrl } /> */}
                        <View style={ styles.knockingParticipantListDetails }>
                            <Text style={ styles.knockingParticipantListText }>
                                { p.name }
                            </Text>
                            { p.email && (
                                <Text style={ styles.knockingParticipantListText }>
                                    { p.email }
                                </Text>
                            ) }
                        </View>
                        <TouchableOpacity
                            onPress={ () => {
                                this.props.admitParticipant(this.props.meeting.meetingId, p.code);
                            } }
                            style={ [
                                styles.knockingParticipantListButton,
                                styles.knockingParticipantListPrimaryButton
                            ] }>
                            <Text style={ styles.knockingParticipantListText }>
                                Admit
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={ () => {
                                this.props.rejectParticipant(this.props.meeting.meetingId, p.code);
                            } }
                            style={ [
                                styles.knockingParticipantListButton,
                                styles.knockingParticipantListSecondaryButton
                            ] }>
                            <Text style={ styles.knockingParticipantListText }>
                                Decline
                            </Text>
                        </TouchableOpacity>
                    </View>
                )) }
            </ScrollView>
        );
    }

    _onRespondToParticipant: (string, boolean) => Function;
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @returns {Props}
 */
function _mapStateToProps(state: Object): $Shape<Props> {
    const abstractProps = abstractMapStateToProps(state);
    const user = state['features/base/settings'].user || state['features/welcome'].user;

    return {
        ...abstractProps,

        // On mobile we only show a portion of the list for screen real estate reasons
        _participants: abstractProps._participants.slice(0, 2),
        waitingParticipantsList: state['features/toolbox'].waitingParticipantsList.slice(0, 2),
        meeting: state['features/welcome'].meeting,
        isHost: typeof user !== 'undefined' && user !== null ? user.isHost : false,
    };
}

const mapDispatchToProps = {
    admitParticipant,
    rejectParticipant
};

export default translate(connect(_mapStateToProps, mapDispatchToProps)(KnockingParticipantList));
