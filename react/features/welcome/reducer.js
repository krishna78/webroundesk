// @flow

import { PersistenceRegistry, ReducerRegistry, set } from '../base/redux';

import { SET_SIDEBAR_VISIBLE, SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE } from './actionTypes';

/**
 * The name of the redux store/state property which is the root of the redux
 * state of the feature {@code welcome}.
 */
const STORE_NAME = 'features/welcome';

/**
 * Sets up the persistence of the feature {@code welcome}.
 */
PersistenceRegistry.register(STORE_NAME, {
    defaultPage: true,
    meeting: null,
    user: null,
    unlocked: false,
    onLobby: false,
    participantLogin: false,
    joining: false
});

/**
 * Reduces redux actions for the purposes of the feature {@code welcome}.
 */
ReducerRegistry.register(STORE_NAME, (state = {}, action) => {
    switch (action.type) {
        case SET_SIDEBAR_VISIBLE:
            return set(state, 'sideBarVisible', action.visible);

        case SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE:
            return set(state, 'defaultPage', action.pageIndex);

        case 'MEETING_IDENTIFIED':
            return set(state, 'meeting', action.meeting);

        case 'UPDATE_JOINING':
            return set(state, 'joining', action.joining);

        case 'USER_IDENTIFIED':
            return set(state, 'user', action.user);

        case 'ADD_TO_LOBBY':
            return set(state, 'onLobby', action.onLobby);

        case 'PASSWORD_PROTECTED':
            return set(state, 'unlocked', action.unlocked);

        case 'PARTICIPANT_LOGIN_REQUIRED':
            return set(state, 'participantLogin', action.participantLogin);
    }

    return state;
});
