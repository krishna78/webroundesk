// @flow
import axios from 'axios';

// import { toast } from 'react-toastify';

import {appNavigate} from '../app/actions';
import {getDefaultURL} from '../app/functions';
// import { setRoom } from '../base/conference';
import {updateSettings, getPropertyValue} from '../base/settings';
import {Alert} from 'react-native';
import {DEFAULT_SERVER_URL, DEFAULT_API_URL} from '../base/settings/constants';

import {SET_SIDEBAR_VISIBLE, SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE} from './actionTypes';
import {disconnect} from '../base/connection';
import {setPreferredVideoQuality} from '../video-quality/actions';

declare var interfaceConfig: Object;

if (typeof interfaceConfig === 'undefined') {
    interfaceConfig = {
        API_ENDPOINT: DEFAULT_API_URL
    };
}

export function notifyMessage(msg: string) {
    Alert.alert('Error', msg);
}

/**
 * Sets the visibility of {@link WelcomePageSideBar}.
 *
 * @param {boolean} visible - If the {@code WelcomePageSideBar} is to be made
 * visible, {@code true}; otherwise, {@code false}.
 * @returns {{
 *     type: SET_SIDEBAR_VISIBLE,
 *     visible: boolean
 * }}
 */
export function setSideBarVisible(visible: boolean) {
    return {
        type: SET_SIDEBAR_VISIBLE,
        visible
    };
}

/**
 * Sets the default page index of {@link WelcomePageLists}.
 *
 * @param {number} pageIndex - The index of the default page.
 * @returns {{
 *     type: SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE,
 *     pageIndex: number
 * }}
 */
export function setWelcomePageListsDefaultPage(pageIndex: number) {
    return {
        type: SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE,
        pageIndex
    };
}

const fetchMeetingRequest = () => {
    return {
        type: 'FETCH_MEETING_START'
    };
};

const fetchMeetingSuccess = (meeting: Object) => {
    return {
        type: 'MEETING_IDENTIFIED',
        meeting
    };
};

const fetchUserSuccess = (user: Object) => {
    return {
        type: 'USER_IDENTIFIED',
        user
    };
};

const updateLockStatus = (unlocked: boolean) => {
    return {
        type: 'PASSWORD_PROTECTED',
        unlocked
    };
};

const updateLobby = (onLobby: boolean) => {
    return {
        type: 'ADD_TO_LOBBY',
        onLobby
    };
};

const updateParticipantLogin = (participantLogin: boolean) => {
    return {
        type: 'PARTICIPANT_LOGIN_REQUIRED',
        participantLogin
    };
};

const updateJoining = (joining: boolean) => {
    return {
        type: 'UPDATE_JOINING',
        joining
    };
};

const generateMeetingLink = (meeting: Object, state: Object) => {
    const audioMuted = Boolean(getPropertyValue(state, 'startWithAudioMuted'));
    const videoMuted = Boolean(getPropertyValue(state, 'startWithVideoMuted'));

    const link = `${DEFAULT_SERVER_URL}/${meeting.roomId}/${meeting.meetingId}`;
    return link;
};

export function confirmPassword(
    meeting: Object,
    user: Object,
    password: string,
    name: string,
    onLobby: boolean,
    navigate: Function
) {
    return async function (dispatch: Function, getState: Function) {
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/confirm-password`, {
                meetingId: meeting.meetingId,
                password
            })
            .then(() => {
                dispatch(updateLockStatus(true));
                // login
                if (user && !onLobby && meeting.isStarted) {
                    const link = generateMeetingLink(meeting, getState());

                    navigate();
                    dispatch(appNavigate(link));
                } else {
                    if (user) {
                        dispatch(loginAsParticipant(name, meeting, user));
                    } else {
                        dispatch(loginAsParticipant(name, meeting));
                    }
                }
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                notifyMessage(message);
            });
    };
}

export function cleanUp() {
    return (dispatch: Function) => {
        dispatch(updateLockStatus(true));
        dispatch(updateLobby(false));
        dispatch(fetchMeetingSuccess(null));
        dispatch(fetchUserSuccess(null));
        dispatch(updateLockStatus(false));
    };
}

export function loginAsParticipant(name: string, meeting: Object, navigate: Function, user = null) {
    return async function (dispatch: Function, getState: Function) {
        if (user === null) {
            axios
                .post(`${interfaceConfig.API_ENDPOINT}/login`, {
                    displayName: name,
                    meetingId: meeting.meetingId
                })
                .then(res => {
                    const {user, meeting, settings} = res.data;

                    dispatch(fetchUserSuccess(user));
                    dispatch(updateSettings({displayName: user.name}));
                    dispatch(
                        fetchMeetingSuccess({
                            ...meeting,
                            user,
                            settings
                        })
                    );
                    dispatch(updateParticipantLogin(false));
                    if (meeting.lockRoom && user.waitingLobby) {
                        // add to waiting list
                        dispatch(updateLobby(true));
                        dispatch(appNavigate('/'));
                    } else if (!user.rejectedByHost) {
                        dispatch(updateLobby(false));
                        if (meeting.isStarted && !meeting.passwordProtected) {
                            const link = generateMeetingLink(meeting, getState());

                            navigate();
                            dispatch(appNavigate(link));
                        } else {
                            dispatch(appNavigate('/'));
                        }
                    } else if (user.rejectedByHost) {
                        dispatch(appNavigate(undefined));
                        dispatch(updateSettings({meeting: null}));
                        dispatch(updateSettings({user: null}));
                    }
                })
                .catch(err => {
                    let message = err.message;

                    if (err.response) {
                        const data = err.response.data;

                        if (data && typeof data.message === 'string' && data.message !== '') {
                            message = data.message;
                        }
                    }

                    notifyMessage(message);
                });
        } else {
            dispatch(updateParticipantLogin(false));
            if (meeting.lockRoom && user.waitingLobby) {
                // add to waiting list
                dispatch(updateLobby(true));
                dispatch(appNavigate('/'));
            } else if (!user.rejectedByHost) {
                dispatch(updateLobby(false));
                if (meeting.isStarted && !meeting.passwordProtected) {
                    const link = generateMeetingLink(meeting, getState());

                    navigate();
                    dispatch(appNavigate(link));
                } else {
                    dispatch(appNavigate('/'));
                }
            } else if (user.rejectedByHost) {
                dispatch(appNavigate(undefined));
                dispatch(updateSettings({meeting: null}));
                dispatch(updateSettings({user: null}));
            }
        }
    };
}

export function loginAsHost(email: string, password: string, meeting: Object, navigate: Function) {
    return async function (dispatch: Function, getState: Function) {
        dispatch(disconnect(true));
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/login`, {
                email,
                password,
                meetingId: meeting.meetingId
            })
            .then(res => {
                const data = res.data;
                const newMeeting = data.meeting;

                dispatch(fetchUserSuccess(data.user));
                dispatch(updateSettings({displayName: data.user.name}));
                dispatch(
                    fetchMeetingSuccess({
                        ...newMeeting,
                        user: data.user,
                        isStarted: true,
                        settings: data.settings
                    })
                );
                const link = generateMeetingLink(meeting, getState());

                navigate();
                dispatch(appNavigate(link));
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                notifyMessage(message);
            });
    };
}

export function setVideoProfile(profile) {
    return (dispatch: Function, getState: Function) => {
        if (profile === 'Low bandwidth') {
            dispatch(setAudioOnly(false));
        } else if (profile === 'Low definition') {
            dispatch(setPreferredVideoQuality(180));
        } else if (profile === 'Standard definition') {
            dispatch(setPreferredVideoQuality(360));
        } else {
            dispatch(setPreferredVideoQuality(720));
        }
    };
}

/**
 * Get meeting infromation.
 *
 * @param {string} room  - Room name.
 * @param {*} token - Join token.
 * @param {*} cb - Navigation call back.
 * @returns {Function}.
 */
export function fetchMeeting(room: string, token: string, navigate: Function) {
    return (dispatch: Function, getState: Function) => {
        //dispatch(fetchUserSuccess(null));
        dispatch(updateJoining(true));
        //notifyMessage('fetching meeting');

        dispatch(updateSettings({serverURL: DEFAULT_SERVER_URL}));

        const dt = new Date();

        axios
            .post(`${interfaceConfig.API_ENDPOINT}/meeting-info`, {
                token,
                meetingOrRoomId: room,
                timeZoneOffset: dt.getTimezoneOffset()
            })
            .then(res => {
                dispatch(updateJoining(false));

                const meeting = res.data;
                const user = meeting.user;

                dispatch(fetchMeetingSuccess(meeting));

                // ask password
                if (!meeting.passwordProtected) {
                    dispatch(updateLockStatus(true));
                }

                const link = generateMeetingLink(meeting, getState());

                if (user) {
                    if (typeof user.videoProfile !== 'undefined') {
                        dispatch(setVideoProfile(user.videoProfile));
                    }
                    if (
                        typeof user.rejectedByHost !== 'undefined' &&
                        user.rejectedByHost !== null &&
                        user.rejectedByHost
                    ) {
                        notifyMessage('Host declined your join request');
                        dispatch(fetchMeetingSuccess(null));
                        dispatch(appNavigate(undefined));
                        dispatch(cleanUp());
                    } else {
                        dispatch(updateParticipantLogin(false));
                        dispatch(fetchUserSuccess(user));
                        // dispatch(updateSettings({ displayName: user.name }));

                        if (
                            meeting.lockRoom &&
                            user.waitingLobby &&
                            meeting.userType !== 'host' &&
                            meeting.isStarted
                        ) {
                            //notifyMessage('1');

                            dispatch(updateLobby(true));
                            dispatch(appNavigate('/'));
                        } else if (
                            meeting.lockRoom &&
                            !user.waitingLobby &&
                            meeting.userType !== 'host' &&
                            meeting.isStarted &&
                            !user.rejectedByHost
                        ) {
                            //notifyMessage('2');
                            dispatch(updateLobby(false));
                            navigate();
                            dispatch(appNavigate(link));
                        } else if (meeting.userType === 'host' && !meeting.passwordProtected) {
                            //notifyMessage('3');
                            dispatch(updateLobby(false));
                            navigate();
                            dispatch(appNavigate(link));
                        } else if (
                            !meeting.passwordProtected &&
                            meeting.isStarted &&
                            !user.rejectedByHost
                        ) {
                            //notifyMessage('4');
                            navigate();
                            dispatch(appNavigate(link));
                        } else if (
                            meeting.passwordProtected &&
                            meeting.isStarted &&
                            !user.rejectedByHost
                        ) {
                            const {unlocked} = getState()['features/welcome'];
                            if (unlocked) {
                                //notifyMessage('5');
                                navigate();
                                dispatch(appNavigate(link));
                            } else {
                                dispatch(appNavigate('/'));
                                //notifyMessage('6');
                            }
                        } else if (user.rejectedByHost) {
                            dispatch(appNavigate(undefined));
                            dispatch(updateSettings({meeting: null}));
                            dispatch(updateSettings({user: null}));
                        } else {
                            dispatch(updateLobby(false));
                            dispatch(appNavigate('/'));
                        }
                    }
                } else {
                    dispatch(updateParticipantLogin(true));

                    // dispatch(appNavigate('/'));
                }
            })
            .catch(err => {
                dispatch(updateJoining(false));
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(updateSettings({serverURL: 'https://meet.roundesk.io'}));
                //notifyMessage(message);
                const link = `https://meet.roundesk.io/${room}`;

                navigate();
                dispatch(appNavigate(link));
            });
    };
}

export function endMeeting(
    meetingId: string,
    data: Object,
    from: string,
    appNavigate: Function,
    disconnect: Function
) {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/end-meeting`, {
                meetingId,
                data,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(() => {
                if (from === 'ReactNative') {
                    dispatch(appNavigate(undefined));
                } else {
                    dispatch(disconnect(true));
                }
                dispatch(cleanUp());
            })
            .catch(() => {
                if (from === 'ReactNative') {
                    dispatch(appNavigate(undefined));
                } else {
                    dispatch(disconnect(true));
                }
                dispatch(cleanUp());
            });
    };
}
