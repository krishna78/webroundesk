import React from 'react';
import {
    Animated,
    Keyboard,
    SafeAreaView,
    KeyboardAvoidingView,
    TouchableHighlight,
    TouchableOpacity,
    View,
    ActivityIndicator,
    Platform
} from 'react-native';
import {Alert} from 'react-native';

import {getName} from '../../app/functions';
import {appNavigate} from '../../app/actions';
import {ColorSchemeRegistry} from '../../base/color-scheme';
import {translate} from '../../base/i18n';
import {Icon, IconMenu, IconWarning} from '../../base/icons';
import {MEDIA_TYPE} from '../../base/media';
import {Header, LoadingIndicator, Text} from '../../base/react';
import {connect} from '../../base/redux';
import {ColorPalette} from '../../base/styles';
import {createDesiredLocalTracks, destroyLocalTracks} from '../../base/tracks';
import {HelpView} from '../../help';
import {DialInSummary} from '../../invite';
import {SettingsView} from '../../settings';
import {
    setSideBarVisible,
    confirmPassword,
    cleanUp,
    loginAsHost,
    loginAsParticipant,
    fetchMeeting
} from '../actions.native';

import {
    AbstractWelcomePage,
    _mapStateToProps as _abstractMapStateToProps
} from './AbstractWelcomePage';
import LocalVideoTrackUnderlay from './LocalVideoTrackUnderlay';
import VideoSwitch from './VideoSwitch';
import WelcomePageLists from './WelcomePageLists';
import WelcomePageSideBar from './WelcomePageSideBar';
import styles, {PLACEHOLDER_TEXT_COLOR} from './styles';
import TextInput from './TextInput';
import Background from './Background';
import {getLocalParticipant, getParticipantDisplayName} from '../../base/participants';
import {updateSettings, getAppToken} from '../../base/settings';
import {lock} from '../../prejoin/actions';

/**
 * The native container rendering the welcome page.
 *
 * @extends AbstractWelcomePage
 */
class WelcomePage extends AbstractWelcomePage {
    /**
     * Constructor of the Component.
     *
     * @inheritdoc
     */
    constructor(props) {
        super(props);

        this.state._fieldFocused = true;
        this.state.hintBoxAnimation = new Animated.Value(0);
        this.state.meetingPassword = '';
        this.state.email = '';
        this.state.password = '';
        this.state.hostLogin = false;

        // Bind event handlers so they are only bound once per instance.
        this._onFieldFocusChange = this._onFieldFocusChange.bind(this);
        this._onShowSideBar = this._onShowSideBar.bind(this);
        this._renderHintBox = this._renderHintBox.bind(this);

        // Specially bind functions to avoid function definition on render.
        this._onFieldBlur = this._onFieldFocusChange.bind(this, false);
        this._onFieldFocus = this._onFieldFocusChange.bind(this, true);
        this.verifyMeeting = this.verifyMeeting.bind(this);
        this.joinAnother = this.joinAnother.bind(this);
        this.askHostLogin = this.askHostLogin.bind(this);
        this.processHostLogin = this.processHostLogin.bind(this);
        this.processParticipantLogin = this.processParticipantLogin.bind(this);
        this.navigateToMeeting = this.navigateToMeeting.bind(this);
        this._onJoin = this._onJoin.bind(this);
    }

    /**
     * Implements React's {@link Component#componentDidMount()}. Invoked
     * immediately after mounting occurs. Creates a local video track if none
     * is available and the camera permission was already granted.
     *
     * @inheritdoc
     * @returns {void}
     */
    componentDidMount() {
        super.componentDidMount();

        this._updateRoomname();

        const {dispatch, meeting, appUser, onLobby} = this.props;

        if (this.props._settings.startAudioOnly) {
            dispatch(destroyLocalTracks());
        } else {
            // Make sure we don't request the permission for the camera from
            // the start. We will, however, create a video track iff the user
            // already granted the permission.
            navigator.permissions.query({name: 'camera'}).then(response => {
                response === 'granted' && dispatch(createDesiredLocalTracks(MEDIA_TYPE.VIDEO));
            });
        }

        let {token} = this.props;
        if (token === null && appUser != null) {
            token = appUser.code;
        }

        if (
            typeof meeting !== 'undefined' &&
            meeting !== null &&
            (meeting.isStarted === false || onLobby)
        ) {
            setTimeout(() => {
                this.props.dispatch(
                    fetchMeeting(
                        meeting.meetingId,
                        this.props.appUser ? this.props.appUser.code : token,
                        () => {}
                    )
                );
            }, 10000);
        }
    }

    componentDidUpdate(oldProps) {
        const {meeting, appUser, onLobby} = this.props;
        let {token} = this.props;
        if (this.props.appUser != null) {
            token = this.props.appUser.code;
        }

        if (
            (typeof oldProps.meeting === 'undefined' &&
                typeof meeting !== 'undefined' &&
                meeting.isStarted === false) ||
            ((oldProps.onLobby === false || typeof oldProps.onLobby === 'undefined') &&
                onLobby === true &&
                meeting !== null &&
                typeof meeting !== 'undefined' &&
                meeting.isStarted === true)
        ) {
            this.interval = setInterval(() => {
                this.props.dispatch(
                    fetchMeeting(
                        meeting.meetingId,
                        this.props.appUser ? this.props.appUser.code : token,
                        this.navigateToMeeting
                    )
                );
            }, 10000);
        } else if (
            typeof meeting !== 'undefined' &&
            meeting !== null &&
            meeting.isStarted === false
        ) {
            clearInterval(this.interval);
            this.interval = setInterval(() => {
                this.props.dispatch(
                    fetchMeeting(
                        meeting.meetingId,
                        this.props.appUser ? this.props.appUser.code : token,
                        this.navigateToMeeting
                    )
                );
            }, 10000);
        }

        if (onLobby === false && meeting !== null && meeting.isStarted === true) {
            clearInterval(this.interval);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    navigateToMeeting() {
        clearInterval(this.interval);
    }

    _onJoin: () => void;

    /**
     * Handles joining. Either by clicking on 'Join' button
     * or by pressing 'Enter' in room name input field.
     *
     * @protected
     * @returns {void}
     */
    _onJoin() {
        const room = this.state.room || this.state.generatedRoomname;

        // sendAnalytics(
        //     createWelcomePageEvent('clicked', 'joinButton', {
        //         isGenerated: !this.state.room,
        //         room
        //     })
        // );

        if (room) {
            this.setState({joining: true});

            // By the time the Promise of appNavigate settles, this component
            // may have already been unmounted.
            const onAppNavigateSettled = () => this._mounted && this.setState({joining: false});
            this.props.dispatch(lock());

            this.props.dispatch(fetchMeeting(room, this.props.token, onAppNavigateSettled));

            // this.props
            //     .dispatch(appNavigate(room))
            //     .then(onAppNavigateSettled, onAppNavigateSettled);
        }
    }

    /**
     * Implements React's {@link Component#render()}. Renders a prompt for
     * entering a room name.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        // We want to have the welcome page support the reduced UI layout,
        // but we ran into serious issues enabling it so we disable it
        // until we have a proper fix in place. We leave the code here though, because
        // this part should be fine when the bug is fixed.
        //
        // NOTE: when re-enabling, don't forget to uncomment the respective _mapStateToProps line too

        /*
        const { _reducedUI } = this.props;

        if (_reducedUI) {
            return this._renderReducedUI();
        }
        */

        return this._renderFullUI();
    }

    /**
     * Renders the insecure room name warning.
     *
     * @inheritdoc
     */
    _doRenderInsecureRoomNameWarning() {
        return (
            <View style={[styles.messageContainer, styles.insecureRoomNameWarningContainer]}>
                <Icon src={IconWarning} style={styles.insecureRoomNameWarningIcon} />
                <Text style={styles.insecureRoomNameWarningText}>
                    {this.props.t('security.insecureRoomNameWarning')}
                </Text>
            </View>
        );
    }

    /**
     * Constructs a style array to handle the hint box animation.
     *
     * @private
     * @returns {Array<Object>}
     */
    _getHintBoxStyle() {
        return [
            styles.messageContainer,
            styles.hintContainer,
            {
                opacity: this.state.hintBoxAnimation
            }
        ];
    }

    /**
     * Callback for when the room field's focus changes so the hint box
     * must be rendered or removed.
     *
     * @private
     * @param {boolean} focused - The focused state of the field.
     * @returns {void}
     */
    _onFieldFocusChange(focused) {
        focused &&
            this.setState({
                _fieldFocused: true
            });

        Animated.timing(this.state.hintBoxAnimation, {
            duration: 300,
            toValue: focused ? 1 : 0
        }).start(
            animationState =>
                animationState.finished &&
                !focused &&
                this.setState({
                    _fieldFocused: false
                })
        );
    }

    /**
     * Toggles the side bar.
     *
     * @private
     * @returns {void}
     */
    _onShowSideBar() {
        Keyboard.dismiss();
        this.props.dispatch(setSideBarVisible(true));
    }

    /**
     * Renders the hint box if necessary.
     *
     * @private
     * @returns {React$Node}
     */
    _renderHintBox() {
        if (this.state._fieldFocused) {
            const {t} = this.props;

            return (
                <Animated.View style={this._getHintBoxStyle()}>
                    <View style={styles.hintTextContainer}>
                        <Text style={styles.hintText}>{t('welcomepage.roomnameHint')}</Text>
                    </View>
                    <View style={styles.hintButtonContainer}>{this._renderJoinButton()}</View>
                </Animated.View>
            );
        }

        return null;
    }

    /**
     * Renders the join button.
     *
     * @private
     * @returns {ReactElement}
     */
    _renderJoinButton() {
        const {t, joining} = this.props;
        let children;

        if (joining) {
            // TouchableHighlight is picky about what its children can be, so
            // wrap it in a native component, i.e. View to avoid having to
            // modify non-native children.
            children = (
                <View>
                    <LoadingIndicator color={styles.buttonText.color} size='small' />
                </View>
            );
        } else {
            children = <Text style={styles.buttonText}>JOIN</Text>;
        }

        return (
            <TouchableHighlight
                accessibilityLabel={t('welcomepage.accessibilityLabel.join')}
                onPress={this._onJoin}
                style={styles.button}
                underlayColor={ColorPalette.white}>
                {children}
            </TouchableHighlight>
        );
    }

    _renderLoginView() {
        return (
            <>
                <Background>
                    <TextInput
                        placeholder='Email'
                        returnKeyType='next'
                        autoCapitalize='none'
                        autoCompleteType='email'
                        textContentType='emailAddress'
                        keyboardType='email-address'
                        value={this.state.email}
                        onChangeText={text => this.setState({email: text})}
                    />

                    <TextInput
                        placeholder='Password'
                        returnKeyType='done'
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={text => this.setState({password: text})}
                    />
                    <TouchableOpacity onPress={this.processHostLogin} style={styles.fullButton}>
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
                </Background>
            </>
        );
    }

    joinAnother() {
        clearInterval(this.interval);
        this.props.dispatch(cleanUp());
        this.props.dispatch(appNavigate(undefined));
    }

    askHostLogin() {
        this.setState({hostLogin: true});
    }

    processHostLogin() {
        this.props.dispatch(
            loginAsHost(
                this.state.email,
                this.state.password,
                this.props.meeting,
                this.navigateToMeeting
            )
        );
    }

    _renderWaitingView() {
        return (
            <>
                <Background>
                    <ActivityIndicator color='#fff' size='large' />
                    <Text style={styles.whiteText}>
                        Meeting not yet started, wait for host to join.
                    </Text>
                    <TouchableOpacity onPress={this.askHostLogin} style={styles.fullButton}>
                        <Text style={styles.buttonText}>Login as Host</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.joinAnother} style={styles.fullButton}>
                        <Text style={styles.buttonText}>Join another meeting</Text>
                    </TouchableOpacity>
                </Background>
            </>
        );
    }

    _renderLobbyView() {
        return (
            <>
                <Background>
                    <ActivityIndicator color='#fff' size='large' />
                    <Text style={styles.whiteText}>Waiting in lobby</Text>
                </Background>
            </>
        );
    }

    verifyMeeting() {
        this.props.dispatch(
            confirmPassword(
                this.props.meeting,
                this.props.appUser,
                this.state.meetingPassword,
                this.props.name,
                this.props.onLobby,
                this.navigateToMeeting
            )
        );
    }

    _renderPasswordView() {
        return (
            <>
                <Background>
                    <TextInput
                        placeholder='Display Name'
                        returnKeyType='next'
                        value={this.props.name}
                        onChangeText={text =>
                            this.props.dispatch(updateSettings({displayName: text}))
                        }
                    />
                    <TextInput
                        placeholder='Meeting Password'
                        returnKeyType='done'
                        secureTextEntry
                        value={this.state.meetingPassword}
                        onChangeText={text => this.setState({meetingPassword: text})}
                    />
                    <TouchableOpacity onPress={this.verifyMeeting} style={styles.fullButton}>
                        <Text style={styles.buttonText}>Continue</Text>
                    </TouchableOpacity>
                </Background>
            </>
        );
    }

    _renderParticipantLogin() {
        return (
            <>
                <Background>
                    <TextInput
                        placeholder='Display Name'
                        returnKeyType='next'
                        value={this.props.name}
                        onChangeText={text =>
                            this.props.dispatch(updateSettings({displayName: text}))
                        }
                    />
                    <TouchableOpacity
                        onPress={this.processParticipantLogin}
                        style={styles.fullButton}>
                        <Text style={styles.buttonText}>Continue</Text>
                    </TouchableOpacity>
                </Background>
            </>
        );
    }

    processParticipantLogin() {
        const {user} = this.props;
        if (
            typeof user !== 'undefined' &&
            user !== null &&
            user.code &&
            user.code !== '' &&
            user.code !== null
        ) {
            this.props.dispatch(
                loginAsParticipant(
                    this.props.name,
                    this.props.meeting,
                    this.navigateToMeeting,
                    user
                )
            );
        } else {
            this.props.dispatch(
                loginAsParticipant(this.props.name, this.props.meeting, this.navigateToMeeting)
            );
        }
    }

    _renderWelcomeView() {
        const roomnameAccLabel = 'welcomepage.accessibilityLabel.roomname';
        const {_headerStyles, t, meeting} = this.props;

        return (
            <LocalVideoTrackUnderlay style={styles.welcomePage}>
                <View style={_headerStyles.page}>
                    <Header style={styles.header}>
                        <TouchableOpacity onPress={this._onShowSideBar}>
                            <Icon src={IconMenu} style={_headerStyles.headerButtonIcon} />
                        </TouchableOpacity>
                        <VideoSwitch />
                    </Header>
                    <SafeAreaView style={styles.roomContainer}>
                        <View style={styles.joinControls}>
                            <Text style={styles.enterRoomText}>{t('welcomepage.roomname')}</Text>
                            <TextInput
                                accessibilityLabel={t(roomnameAccLabel)}
                                autoCapitalize='none'
                                autoComplete='off'
                                autoCorrect={false}
                                autoFocus={false}
                                onBlur={this._onFieldBlur}
                                onChangeText={this._onRoomChange}
                                onFocus={this._onFieldFocus}
                                onSubmitEditing={this._onJoin}
                                placeholder={this.state.roomPlaceholder}
                                placeholderTextColor={PLACEHOLDER_TEXT_COLOR}
                                returnKeyType={'go'}
                                style={styles.textInput}
                                underlineColorAndroid='transparent'
                                value={this.state.room}
                            />
                            {this._renderHintBox()}
                        </View>
                    </SafeAreaView>
                    {/* <WelcomePageLists disabled={ this.state._fieldFocused } /> */}
                </View>
                <WelcomePageSideBar />
                {this._renderWelcomePageModals()}
            </LocalVideoTrackUnderlay>
        );
    }

    /**
     * Renders the full welcome page.
     *
     * @returns {ReactElement}
     */
    _renderFullUI() {
        const {unlocked, meeting, onLobby, appUser, participantLogin} = this.props;
        if (meeting === null || typeof meeting === 'undefined') {
            return this._renderWelcomeView();
        } else {
            if (participantLogin) {
                return this._renderParticipantLogin();
            } else if (meeting.passwordProtected && !unlocked) {
                return this._renderPasswordView();
            } else if (!meeting.isStarted) {
                if (this.state.hostLogin) {
                    return this._renderLoginView();
                }
                return this._renderWaitingView();
            } else if (onLobby) {
                return this._renderLobbyView();
            } else {
                return this._renderWelcomeView();
                // return (
                //     <>
                //         <SafeAreaView style={ styles.reducedUIContainer }>
                //             <Background>
                //                 <ActivityIndicator color='#fff' size='large' />
                //                 {/* <Text style={ styles.whiteText }>Something went wrong.</Text>
                //                 <Text style={ styles.whiteText }>
                //                     { unlocked ? 'unlocked ' : '!unlocked ' }
                //                     { onLobby ? 'onLobby ' : '!onLobby ' }
                //                     { meeting.isStarted ? 'started ' : '!started ' }
                //                     { this.state.hostLogin ? 'host login ' : '!host login ' }
                //                     { participantLogin ? 'participantLogin ' : '!participantLogin ' }
                //                 </Text> */}
                //             </Background>
                //         </SafeAreaView>
                //     </>
                // );
            }
        }
    }

    /**
     * Renders a "reduced" version of the welcome page.
     *
     * @returns {ReactElement}
     */
    _renderReducedUI() {
        const {t} = this.props;

        return (
            <View style={styles.reducedUIContainer}>
                <Text style={styles.reducedUIText}>
                    {t('welcomepage.reducedUIText', {app: getName()})}
                </Text>
            </View>
        );
    }

    /**
     * Renders JitsiModals that are supposed to be on the welcome page.
     *
     * @returns {Array<ReactElement>}
     */
    _renderWelcomePageModals() {
        return [
            <HelpView key='helpView' />,
            <DialInSummary key='dialInSummary' />,
            <SettingsView key='settings' />
        ];
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @returns {Object}
 */
function _mapStateToProps(state) {
    const _localParticipant = getLocalParticipant(state);
    const _localParticipantId = _localParticipant?.id;
    const _displayName = _localParticipant && getParticipantDisplayName(state, _localParticipantId);
    const participantLogin = state['features/welcome'].participantLogin;
    const unlocked = state['features/welcome'].unlocked;
    const onLobby = state['features/welcome'].onLobby;
    const user = state['features/welcome'].user;
    const meeting = state['features/welcome'].meeting;
    const joining = state['features/welcome'].joining;

    return {
        ..._abstractMapStateToProps(state),
        _headerStyles: ColorSchemeRegistry.get(state, 'Header'),
        meeting,
        appUser: user,
        user,
        onLobby,
        unlocked,
        participantLogin,
        name: _displayName,
        token: getAppToken(state),
        joining
    };
}

export default translate(connect(_mapStateToProps)(WelcomePage));
