/* @flow */

import React, { Component } from 'react';

import { translate } from '../../base/i18n';
import Video from '../../base/media/components/Video';
import { connect } from '../../base/redux';
import { getLocalVideoType, isLocalVideoTrackMuted } from '../../base/tracks';

const VIDEO_ERROR_CLASS = 'video-preview-has-error';

/**
 * The type of the React {@code Component} props of {@link VideoInputPreview}.
 */
type Props = {
    /**
     * An error message to display instead of a preview. Displaying an error
     * will take priority over displaying a video preview.
     */
    error: ?string;

    /**
     * The JitsiLocalTrack to display.
     */
    track: Object;
};

/**
 * React component for displaying video. This component defers to lib-jitsi-meet
 * logic for rendering the video.
 *
 * @extends Component
 */
class VideoInputPreview extends Component<Props> {
    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { error } = this.props;
        const errorClass = error ? VIDEO_ERROR_CLASS : '';
        const className = `video-input-preview ${errorClass}`;
        // const muted = isLocalVideoTrackMuted(this.props.track);

        return (
            <div className={ className }>
                {this.props._videoMuted ? (
                    <div className='camera-preview-disabled' />
                ) : (
                        <Video
                            className='video-input-preview-display flipVideoX'
                            playsinline={ true }
                            videoTrack={ { jitsiTrack: this.props.track } } />
                    ) }
                <div className='video-input-preview-error'>{ error || '' }</div>
            </div>
        );
    }
}

/**
 * Map state to props.
 *
 * @param {*} state - State.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    const { enabled: audioOnly } = state['features/base/audio-only'];
    const tracks = state['features/base/tracks'];
    const _videoMuted = isLocalVideoTrackMuted(tracks);

    return {
        _videoMuted
    };
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(translate(VideoInputPreview));
