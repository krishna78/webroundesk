/* @flow */

import React, { Component } from 'react';

import { translate } from '../../base/i18n';

/**
 * The type of the React {@code Component} props of {@link SpeakerStatsLabels}.
 */
type Props = {
    /**
     * The function to translate human-readable text.
     */
    t: Function;
};

/**
 * React component for labeling speaker stats column items.
 *
 * @extends Component
 */
class SpeakerStatsLabels extends Component<Props> {
    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { t } = this.props;

        return (
            <>
                <thead>
                    <tr className='speaker-stats-item__labels'>
                        <th>
                            <div className='speaker-stats-item__status' />
                        </th>
                        <th>
                            <div className='speaker-stats-item__name'>{ t('speakerStats.name') }</div>
                        </th>
                        <th>
                            <div className='speaker-stats-item__time'>
                                { t('speakerStats.speakerTime') }
                            </div>
                        </th>
                    </tr>
                </thead>
            </>
        );
    }
}

export default translate(SpeakerStatsLabels);
