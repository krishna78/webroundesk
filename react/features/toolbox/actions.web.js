// @flow
import axios from 'axios';
import type {Dispatch} from 'redux';

import {showNotification} from '../notifications/actions';
import {NOTIFICATION_TYPE} from '../notifications/constants';
import {browser, jit} from '../base/lib-jitsi-meet';
import {Platform} from '../base/react';
import {getLocalParticipant} from '../base/participants';
import {getTrackByMediaTypeAndParticipant, getLocalVideoTrack} from '../base/tracks';
import {MEDIA_TYPE, VIDEO_TYPE} from '../base/media';

import {
    FULL_SCREEN_CHANGED,
    SET_FULL_SCREEN,
    INVITE_PARTICIPANT_SUCCESS,
    INVITE_PARTICIPANT_DONE,
    PENDING_LIST_RECIEVED
} from './actionTypes';
import {
    clearToolboxTimeout,
    setToolboxTimeout,
    setToolboxTimeoutMS,
    setToolboxVisible
} from './actions.native';
import {updateSettings} from '../base/settings';

declare var interfaceConfig: Object;

export * from './actions.native';

/**
 * Docks/undocks the Toolbox.
 *
 * @param {boolean} dock - True if dock, false otherwise.
 * @returns {Function}
 */
export function dockToolbox(dock: boolean): Function {
    return (dispatch: Dispatch<any>, getState: Function) => {
        if (interfaceConfig.filmStripOnly) {
            return;
        }

        const {timeoutMS, visible} = getState()['features/toolbox'];

        if (dock) {
            // First make sure the toolbox is shown.
            visible || dispatch(showToolbox());

            dispatch(clearToolboxTimeout());
        } else if (visible) {
            dispatch(setToolboxTimeout(() => dispatch(hideToolbox()), timeoutMS));
        } else {
            dispatch(showToolbox());
        }
    };
}

/**
 * Signals that full screen mode has been entered or exited.
 *
 * @param {boolean} fullScreen - Whether or not full screen mode is currently
 * enabled.
 * @returns {{
 *     type: FULL_SCREEN_CHANGED,
 *     fullScreen: boolean
 * }}
 */
export function fullScreenChanged(fullScreen: boolean) {
    return {
        type: FULL_SCREEN_CHANGED,
        fullScreen
    };
}

/**
 * Hides the toolbox.
 *
 * @param {boolean} force - True to force the hiding of the toolbox without
 * caring about the extended toolbar side panels.
 * @returns {Function}
 */
export function hideToolbox(force: boolean = false): Function {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const state = getState();
        const {alwaysVisible, hovered, timeoutMS} = state['features/toolbox'];

        if (alwaysVisible) {
            return;
        }

        dispatch(clearToolboxTimeout());

        if (
            !force &&
            (hovered || state['features/invite'].calleeInfoVisible || state['features/chat'].isOpen)
        ) {
            dispatch(setToolboxTimeout(() => dispatch(hideToolbox()), timeoutMS));
        } else {
            dispatch(setToolboxVisible(false));
        }
    };
}

/**
 * Signals a request to enter or exit full screen mode.
 *
 * @param {boolean} fullScreen - True to enter full screen mode, false to exit.
 * @returns {{
 *     type: SET_FULL_SCREEN,
 *     fullScreen: boolean
 * }}
 */
export function setFullScreen(fullScreen: boolean) {
    return {
        type: SET_FULL_SCREEN,
        fullScreen
    };
}

/**
 * Shows the toolbox for specified timeout.
 *
 * @param {number} timeout - Timeout for showing the toolbox.
 * @returns {Function}
 */
export function showToolbox(timeout: number = 0): Object {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const state = getState();
        const {alwaysVisible, enabled, timeoutMS, visible} = state['features/toolbox'];

        if (enabled && !visible) {
            dispatch(setToolboxVisible(true));

            // If the Toolbox is always visible, there's no need for a timeout
            // to toggle its visibility.
            if (!alwaysVisible) {
                dispatch(setToolboxTimeout(() => dispatch(hideToolbox()), timeout || timeoutMS));
                dispatch(setToolboxTimeoutMS(interfaceConfig.TOOLBAR_TIMEOUT));
            }
        }
    };
}

/**
 * Invite success.
 *
 * @param {*} message - Message.
 * @returns {*}
 */
export function inviteSuccess(message: string) {
    return {
        type: INVITE_PARTICIPANT_SUCCESS,
        message
    };
}

/**
 * Invite success.
 *
 * @param {*} list - List.
 * @returns {*}
 */
export function pendingListRecieved(list: any) {
    return {
        type: PENDING_LIST_RECIEVED,
        list
    };
}

/**
 * Invite success.
 *
 * @param {*} list - List.
 * @returns {*}
 */
export function waitingListRecieved(list: any) {
    return {
        type: 'WAITING_LIST_RECIEVED',
        list
    };
}

/**
 * Invite success.
 *
 * @param {*} link - Link.
 * @returns {*}
 */
export function guestLinkRecieved(link: string) {
    return {
        type: 'GUEST_LINK_RECIEVED',
        link
    };
}

/**
 * Invite success.
 *
 * @param {*} message - Message.
 * @returns {*}
 */
export function inviteDone() {
    return {
        type: INVITE_PARTICIPANT_DONE
    };
}

/**
 * Send invitation.
 *
 * @param {*} name - Name.
 * @param {*} email - Email.
 * @param {*} meetingId - Meeting ID.
 * @returns {*}
 */
export function sendInvitation(name: string, email: string, meetingId: string): Object {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/send-invite`, {
                meetingId,
                name,
                email,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(res => {
                const data = res.data;
                dispatch(inviteSuccess(data.success));
                dispatch(
                    showNotification(
                        {
                            title: data.success,
                            appearance: NOTIFICATION_TYPE.SUCCESS
                        },
                        5000
                    )
                );
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        5000
                    )
                );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meetingId - Meeting ID.
 * @returns {*}
 */
export function getNonJoinedParticipants(meetingId: string): Object {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/pending-invites`, {
                meetingId,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(res => {
                const data = res.data;
                dispatch(pendingListRecieved(data));
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                // dispatch(
                //     showNotification(
                //         {
                //             title: message,
                //             appearance: NOTIFICATION_TYPE.ERROR
                //         },
                //         5000
                //     )
                // );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meeting - Meeting.
 * @returns {*}
 */
export function getWaitingParticipants(meeting: Object): Object {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;

        axios
            .post(`${interfaceConfig.API_ENDPOINT}/waiting-lobby`, {
                meetingId: meeting.meetingId,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(res => {
                const data = res.data;
                dispatch(waitingListRecieved(data));
                if (meeting.userType === 'host') {
                    let loaded = JSON.parse(sessionStorage.getItem('lobby'));
                    if (loaded === null) {
                        loaded = [];
                    }
                    data.forEach(item => {
                        if (loaded.indexOf(item.code) === -1) {
                            loaded.push(item.code);
                            dispatch(
                                showNotification(
                                    {
                                        title: `${item.name} is waiting on lobby`,
                                        appearance: NOTIFICATION_TYPE.SUCCESS
                                    },
                                    5000
                                )
                            );
                        }
                    });

                    sessionStorage.setItem('lobby', JSON.stringify(loaded));
                }
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                // dispatch(
                //     showNotification(
                //         {
                //             title: message,
                //             appearance: NOTIFICATION_TYPE.ERROR
                //         },
                //         5000
                //     )
                // );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meetingId - Meeting ID.
 * @param {*} code - Code.
 * @returns {*}
 */
export function admitParticipant(meetingId: string, code: string): Object {
    return (dispatch: Dispatch<any>) => {
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/admit-participant`, {
                meetingId,
                code
            })
            .then(res => {
                const data = res.data;
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        5000
                    )
                );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meetingId - Meeting ID.
 * @param {*} code - Code.
 * @returns {*}
 */
export function rejectParticipant(meetingId: string, code: string): Object {
    return (dispatch: Dispatch<any>) => {
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/reject-lobby-user`, {
                meetingId,
                code
            })
            .then(res => {
                const data = res.data;
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        5000
                    )
                );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meetingId - Meeting ID.
 * @param {*} code - Code.
 * @returns {*}
 */
export function generateGuestLink(meetingId: string, code: string): Object {
    return (dispatch: Dispatch<any>) => {
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/guest-participant`, {
                meetingId,
                code
            })
            .then(res => {
                const data = res.data;
                if (data.guestLink) {
                    dispatch(guestLinkRecieved(data.guestLink));
                    navigator.clipboard.writeText(data.guestLink);
                    dispatch(
                        showNotification(
                            {
                                title: 'Guest invite link generated & copied to clipboard.',
                                appearance: NOTIFICATION_TYPE.SUCCESS
                            },
                            5000
                        )
                    );
                }
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        5000
                    )
                );
            });
    };
}

/**
 * Send invitation.
 *
 * @param {*} meeting - Meeting.
 * @returns {*}
 */
export function lockMeetingRoom(meeting: Object, locked: boolean): Object {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/amend-settings`, {
                meetingId: meeting.meetingId,
                code: meeting.user.code,
                lockRoom: locked,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(res => {
                const data = res.data;

                dispatch(
                    updateSettings({
                        meeting: {
                            ...meeting,
                            lockRoom: locked
                        }
                    })
                );

                if (locked) {
                    dispatch(
                        showNotification(
                            {
                                title: 'Meeting room locked!',
                                appearance: NOTIFICATION_TYPE.SUCCESS
                            },
                            5000
                        )
                    );
                } else {
                    dispatch(
                        showNotification(
                            {
                                title: 'Meeting room unlocked!',
                                appearance: NOTIFICATION_TYPE.SUCCESS
                            },
                            5000
                        )
                    );
                }
            })
            .catch(err => {
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        5000
                    )
                );
            });
    };
}

export function updateRecorder(recorder: Object) {
    return {
        type: 'UPDATE_RECORDER',
        recorder
    };
}

export function updateStats(stats: Object) {
    return {
        type: 'UPDATE_STATS',
        stats
    };
}

export function updateAudioRecorder(recorder: Object) {
    return {
        type: 'UPDATE_AUDIO_RECORDER',
        recorder
    };
}

export function trackEvent(appEvent) {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const tracks = getState()['features/base/tracks'];
        const id = getLocalParticipant(getState()).id;
        const audioTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.AUDIO, id);
        const videoTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.VIDEO, id);
        const localVideo = getLocalVideoTrack(getState()['features/base/tracks']);

        const data = {
            user: getState()['features/base/settings'].user || getState()['features/welcome'].user,
            meeting:
                getState()['features/welcome'].meeting ||
                getState()['features/base/settings'].meeting,
            link: getState()['features/base/connection'].locationURL,
            // participants: getState()['features/base/participants'],
            browser: {name: browser._name, version: browser._version, product: navigator.product},
            os: {name: Platform.OS, version: Platform.Version},
            devicePermissions: jit.mediaDevices._grantedPermissions,
            isWebRtcSupported: jit.isWebRtcSupported(),
            isMultipleAudioInputSupported: jit.isMultipleAudioInputSupported(),
            isDeviceChangeAvailable: jit.isDeviceChangeAvailable(),
            isDeviceListAvailable: jit.isDeviceListAvailable(),
            isDesktopSharingEnabled: jit.isDesktopSharingEnabled(),
            availableDevices: getState()['features/base/devices'].availableDevices,
            enabledAudioOnly: getState()['features/base/audio-only'].enabled,
            screenSharing: localVideo && localVideo.videoType === 'desktop',
            timestamp: new Date(),
            videoMuted: !videoTrack || videoTrack.muted,
            audioMuted: audioTrack?.muted ?? true,
            network: getState()['features/toolbox'].stats
        };

        if (data.meeting && data.meeting.meetingId) {
            axios
                .post(`${interfaceConfig.API_ENDPOINT}/log-events`, {
                    meetingId: data.meeting.meetingId,
                    eventName: appEvent,
                    data
                })
                .then(res => {
                    console.log(res);
                })
                .catch(err => {
                    let message = err.message;

                    if (err.response) {
                        const data = err.response.data;

                        if (data && typeof data.message === 'string' && data.message !== '') {
                            message = data.message;
                        }
                    }

                    console.log(message);
                });
        }
    };
}

export function pollEvent() {
    return (dispatch: Dispatch<any>, getState: Function) => {
        const tracks = getState()['features/base/tracks'];
        const id = getLocalParticipant(getState()).id;
        const audioTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.AUDIO, id);
        const videoTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.VIDEO, id);
        const localVideo = getLocalVideoTrack(getState()['features/base/tracks']);
        const conference = getState()['features/base/conference'];
        if (typeof conference !== 'undefined' && typeof conference.conference !== 'undefined') {
            const data = {
                user:
                    getState()['features/base/settings'].user ||
                    getState()['features/welcome'].user,
                conferenceTimeStamp: conference.conferenceTimestamp,
                meetingId: conference.room,
                speakerStats: conference.conference.speakerStatsCollector.stats.users,
                analytics: getState()['features/analytics'],
                link: getState()['features/base/connection'].locationURL,
                participants: getState()['features/base/participants'],
                chat: getState()['features/chat'].messages,
                video: getState()['features/shared-video'],
                lobby: getState()['features/toolbox'].waitingParticipantsList,
                nonJoined: getState()['features/toolbox'].nonJoinedParticipantsList,
                endedOn: new Date()
            };

            const temp = [];

            data.participants.forEach(element => {
                let picked = (({
                    unlocked,
                    network,
                    name,
                    local,
                    joinTime,
                    id,
                    email,
                    dominantSpeaker,
                    connectionStatus,
                    code,
                    browser
                }) => ({
                    unlocked,
                    network,
                    name,
                    local,
                    joinTime,
                    id,
                    email,
                    dominantSpeaker,
                    connectionStatus,
                    code,
                    browser
                }))(element);
                temp.push(picked);
            });

            data.participants = temp;
            const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;

            axios
                .post(`${interfaceConfig.API_ENDPOINT}/end-meeting`, {
                    meetingId: data.meetingId,
                    data,
                    token: typeof user !== 'undefined' ? user.code : null
                })
                .then(res => {
                    console.log(res);
                })
                .catch(err => {
                    let message = err.message;

                    if (err.response) {
                        const data = err.response.data;

                        if (data && typeof data.message === 'string' && data.message !== '') {
                            message = data.message;
                        }
                    }

                    console.log(message);
                });
        }
    };
}
