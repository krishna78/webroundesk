// @flow
// global interfaceConfig;
import {faDropbox, faYoutube} from '@fortawesome/free-brands-svg-icons';
import {
    faTimes,
    faTachometerAlt,
    faUserPlus,
    faDesktop,
    faCommentAlt,
    faFolderOpen,
    faPaperPlane,
    faUser,
    faShareAlt,
    faEnvelope,
    faHandshake
} from '@fortawesome/free-solid-svg-icons';
import {browser} from '../../../../features/base/lib-jitsi-meet';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import React, {Component} from 'react';
import {Tab, Tabs, Accordion, Button} from 'react-bootstrap';
import {translate} from '../../../base/i18n';
import {getLocalParticipant} from '../../../base/participants';
import {connect} from '../../../base/redux';
import {getMeetingDetails} from '../../../base/settings';
import {getLocalVideoTrack, toggleScreensharing} from '../../../base/tracks';
import {NewChat} from '../../../chat';
import {showNotification} from '../../../notifications/actions';
import {LiveStreamButton} from '../../../recording'; // RecordButton

import {muteAllParticipants} from '../../../remote-video-menu/actions';
import {SpeakerStatsList} from '../../../speaker-stats';
import {VideoQualitySlider} from '../../../video-quality';
import {VideoShareButton} from '../../../youtube-player/components';
import {
    sendInvitation,
    getNonJoinedParticipants,
    getWaitingParticipants,
    admitParticipant,
    rejectParticipant,
    generateGuestLink,
    lockMeetingRoom,
    updateRecorder
} from '../../actions.web';
import InviteParticipants from './InviteParticipants';
import {ReactMediaRecorder} from 'react-media-recorder';
import {pollEvent, trackEvent} from '../../../toolbox/actions.web';

type State = {
    activeTab: string;
    allMuted: boolean;
};
type Props = {
    isOpen: boolean;
    onClose: Function;
    conference: Object;
    meeting: Object;
    dispatch: Function;
    sendInvitation: Function;
};
/**
 * Meeting info
 */

class SettingsSidebar extends Component<Props, State> {
    /**
     * Component constructor.
     *
     * @param {*} props - Props.
     * @returns {void}.
     */
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 'dashboard',
            allMuted: false
        };
        this.muteAll = this.muteAll.bind(this);
        this.admitParticipant = this.admitParticipant.bind(this);
        this.rejectParticipant = this.rejectParticipant.bind(this);
        this.generateGuestLink = this.generateGuestLink.bind(this);
        this.toggleLock = this.toggleLock.bind(this);
        this.copyLink = this.copyLink.bind(this);
        this.downloadRecording = this.downloadRecording.bind(this);
        this.startRecording = this.startRecording.bind(this);
        this.stopRecording = this.stopRecording.bind(this);
    }

    componentDidMount() {
        const {meeting, isHost} = this.props;

        if (typeof meeting !== 'undefined' && meeting !== null) {
            this.props.trackEvent('ENTERED_MEETING_ROOM');
            if (isHost) {
                this.props.getNonJoinedParticipants(meeting.meetingId);
                this.props.getWaitingParticipants(meeting);
                this.props.pollEvent();
            }
            this.interval = setInterval(() => {
                if (isHost) {
                    this.props.getNonJoinedParticipants(meeting.meetingId);
                    this.props.getWaitingParticipants(meeting);
                }
            }, 5000);

            this.intervalOne = setInterval(() => {
                if (isHost) {
                    this.props.pollEvent();
                }
            }, 20000);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        clearInterval(this.intervalOne);
    }
    /**
     * Mute.
     *
     * @returns {void}
     */

    muteAll() {
        this.setState({
            allMuted: true
        });
        this.props.muteAllParticipants(this.props.localParticipantId).then(() => {
            this.props.showNotification(
                {
                    title: 'Muted everyone else!'
                },
                5000
            );
            setTimeout(() => {
                this.setState({
                    allMuted: false
                });
            }, 6000);
        });
    }
    /**
     * Mute.
     *
     * @param {string} code - Code.
     * @returns { void}
     */

    admitParticipant(code) {
        this.props.admitParticipant(this.props.meeting.meetingId, code).then(() => {
            this.props.getNonJoinedParticipants(this.props.meeting.meetingId);
            this.props.getWaitingParticipants(this.props.meeting);
        });
    }
    /**
     * Mute.
     *
     * @param {string} code - Code.
     * @returns { void}
     */

    rejectParticipant(code) {
        this.props.rejectParticipant(this.props.meeting.meetingId, code).then(() => {
            this.props.getNonJoinedParticipants(this.props.meeting.meetingId);
            this.props.getWaitingParticipants(this.props.meeting);
        });
    }
    /**
     * Mute.
     *
     * @param {string} code - Code.
     * @returns { void}
     */

    generateGuestLink() {
        this.props
            .generateGuestLink(this.props.meeting.meetingId, this.props.meeting.user.code)
            .then(() => {});
    }

    downloadRecording(blobUrl) {
        const {meeting} = this.props;
        if (meeting && meeting !== null) {
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = blobUrl;
            if (MediaRecorder.isTypeSupported('video/mp4')) {
                a.download = `${meeting.meetingId}-screen-recording.mp4`;
            } else if (MediaRecorder.isTypeSupported('video/webm')) {
                a.download = `${meeting.meetingId}-screen-recording.webm`;
            } else {
                a.download = `${meeting.meetingId}-screen-recording.webm`;
            }

            document.body.appendChild(a);
            a.click();
            setTimeout(function () {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(blobUrl);
            }, 100);
        }
    }

    /**
     * Mute.
     *
     * @param {string} e - Event.
     * @returns { void}
     */

    toggleLock(e) {
        const locked = e.target.value === 'true' ? false : true;
        this.props.lockMeetingRoom(this.props.meeting, locked);
    }

    async startRecording() {
        let recorder;

        // if (this.props.recorder !== null) {
        //     recorder = this.props.recorder;
        // } else {

        let audioTrack, videoTrack, stream;
        navigator.mediaDevices
            .getDisplayMedia({video: true, audio: true})
            .then(async displayStream => {
                [videoTrack] = displayStream.getVideoTracks();
                const audioStream = await navigator.mediaDevices
                    .getUserMedia({audio: true})
                    .catch(e => {
                        throw e;
                    });
                [audioTrack] = audioStream.getAudioTracks();
                // displayStream.addTrack(audioTrack); // do stuff
                // or
                stream = new MediaStream([videoTrack, audioTrack]);

                let options;
                if (MediaRecorder.isTypeSupported('video/mp4')) {
                    options = {mimeType: 'video/mp4'};
                } else if (MediaRecorder.isTypeSupported('video/webm')) {
                    options = {mimeType: 'video/webm'};
                } else {
                    options = null;
                }

                recorder = new MediaRecorder(stream, options);

                const chunks = [];

                recorder.ondataavailable = e => chunks.push(e.data);
                recorder.start();

                recorder.onstop = e => {
                    let completeBlob;
                    if (MediaRecorder.isTypeSupported('video/mp4')) {
                        completeBlob = new Blob(chunks, {type: 'video/mp4'});
                    } else if (MediaRecorder.isTypeSupported('video/webm')) {
                        completeBlob = new Blob(chunks, {type: 'video/webm'});
                    } else {
                        completeBlob = new Blob(chunks, {type: chunks[0].type});
                    }
                    // const completeBlob = new Blob(chunks, { type: 'video/mp4' });

                    this.downloadRecording(URL.createObjectURL(completeBlob));
                    this.props.updateRecorder(recorder);

                    stream.getTracks().forEach(track => track.stop());
                };

                stream.getVideoTracks()[0].onended = () => {
                    // let completeBlob;
                    // if (MediaRecorder.isTypeSupported('video/mp4')) {
                    //     completeBlob = new Blob(chunks, { type: 'video/mp4' });
                    // } else if (MediaRecorder.isTypeSupported('video/webm')) {
                    //     completeBlob = new Blob(chunks, { type: 'video/webm' });
                    // } else {
                    //     completeBlob = new Blob(chunks, { type: chunks[0].type });
                    // }
                    // // const completeBlob = new Blob(chunks, { type: 'video/mp4' });

                    // this.downloadRecording(URL.createObjectURL(completeBlob));
                    // this.props.updateRecorder(recorder);

                    stream.getTracks().forEach(track => track.stop());
                };

                this.props.updateRecorder(recorder);
            })
            .catch(console.error);

        // const stream = await navigator.mediaDevices.getDisplayMedia({
        //     video: { mediaSource: 'screen', width: 1024, height: 576 },
        //     audio: true
        // });

        /*
        {
                deviceId: {
                    exact: await navigator.mediaDevices.enumerateDevices()
                        .then(devices =>
                            devices.find(({
                                kind, label, groupId
                            }) => label === "Monitor of Built-in Audio Analog Stereo" // Firefox
                                || kind === "audiooutput" && groupId !== "default" // Chromium
                            ))
                        .deviceId
                }
            }
         */

        // }
    }

    stopRecording() {
        if (this.props.recorder !== null) {
            this.props.recorder.stop();
        }
    }

    copyLink() {
        navigator.clipboard.writeText(this.props.guestLink);
        this.props.showNotification(
            {
                title: 'Link copied to clipboard'
            },
            5000
        );
    }

    /**
     * Render.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const {
            onInvite,
            admitParticipant,
            rejectParticipant,
            generateGuestLink,
            toggleLock,
            copyLink,
            startRecording,
            stopRecording
        } = this;
        const {
            setTab,
            isOpen,
            onClose,
            conference,
            meeting,
            activeTab,
            guestLink,
            isHost,
            waitingParticipantsList
        } = this.props;

        return meeting && meeting.meetingId ? (
            <div
                className={isOpen ? 'card card-right open' : 'card card-right'}
                id='settings-side-nav'>
                <div className='card-header'>
                    {meeting.meetingName} {meeting.meetingId}
                    <div className='float-right'>
                        <button
                            className='btn btn-link text-white btn-icon'
                            onClick={onClose}
                            type='button'>
                            <FontAwesomeIcon icon={faTimes} size='sm' />
                        </button>
                    </div>
                </div>
                <div className='card-body'>
                    <Tabs activeKey={activeTab} id='controlled-tab' onSelect={k => setTab(k)}>
                        <Tab
                            eventKey='dashboard'
                            title={<FontAwesomeIcon icon={faTachometerAlt} size='lg' />}>
                            <Accordion defaultActiveKey='1'>
                                {this.props.nonJoinedParticipantsList.length > 0 && isHost ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey='1'
                                                variant='link'>
                                                Participants - Yet to Join
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey='1'>
                                            <div className='card-body radius-5'>
                                                {this.props.nonJoinedParticipantsList.map(
                                                    (item, index) => (
                                                        <div
                                                            className='list-group-item'
                                                            key={index}>
                                                            <div className='row'>
                                                                <div
                                                                    className='col-md-1'
                                                                    style={{
                                                                        marginTop: '-9px'
                                                                    }}>
                                                                    <FontAwesomeIcon
                                                                        className='mt-3'
                                                                        icon={faUser}
                                                                        size='lg'
                                                                    />
                                                                </div>
                                                                <div className='col-md-10'>
                                                                    <h6 className='mb-0'>
                                                                        {item.name}{' '}
                                                                    </h6>

                                                                    <div className=''>
                                                                        <small>
                                                                            {item.email
                                                                                ? item.email
                                                                                : 'NIL'}
                                                                        </small>
                                                                        {/* <FontAwesomeIcon
                              className='ml-3'
                              icon={ faEnvelope }
                              size='sm' /> */}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                )}
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}
                                {waitingParticipantsList.length > 0 && isHost ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey='2'
                                                variant='link'>
                                                Waiting Lobby
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey='2'>
                                            <div className='card-body radius-5'>
                                                {waitingParticipantsList.map((item, index) => (
                                                    <div className='list-group-item' key={index}>
                                                        <div className='row'>
                                                            <div className='col-md-1'>
                                                                <FontAwesomeIcon
                                                                    className='mt-2 mr-2'
                                                                    icon={faUser}
                                                                    size='lg'
                                                                />
                                                            </div>
                                                            <div className='col-md-10'>
                                                                <h6>{item.name}</h6>
                                                                <div>
                                                                    <button
                                                                        type='button'
                                                                        className='btn btn-danger btn-sm mr-2'
                                                                        onClick={() => {
                                                                            rejectParticipant(
                                                                                item.code
                                                                            );
                                                                        }}>
                                                                        Decline
                                                                    </button>
                                                                    <button
                                                                        type='button'
                                                                        onClick={() => {
                                                                            admitParticipant(
                                                                                item.code
                                                                            );
                                                                        }}
                                                                        className='btn btn-primary btn-sm'>
                                                                        Admit
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}
                                {meeting && meeting.settings.muteEveryoneElse ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={3}
                                                variant='link'>
                                                Mute Everyone
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={3}>
                                            <div className='card-body p-0'>
                                                <div className='list-group-item '>
                                                    <div className='row'>
                                                        <div className='col-md-12'>
                                                            <h6 className='mb-0'>
                                                                Mute everyone except yourself?
                                                            </h6>
                                                            <small>
                                                                Once muted, you won't be able to
                                                                unmute them, but they can unmute
                                                                themselves at any time.
                                                            </small>
                                                            <div className='mt-3'>
                                                                <button
                                                                    className='btn btn-primary mr-2'
                                                                    disabled={this.state.allMuted}
                                                                    onClick={this.muteAll}
                                                                    type='button'>
                                                                    Mute
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}
                                {meeting && meeting.settings.speakerState ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={4}
                                                variant='link'>
                                                Speaker Status
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={4}>
                                            <div className='card-body p-0'>
                                                <div className='list-group-item '>
                                                    <div className='row'>
                                                        <div className='col-md-12'>
                                                            {typeof conference === 'undefined' ? (
                                                                ''
                                                            ) : (
                                                                <SpeakerStatsList
                                                                    conference={conference}
                                                                />
                                                            )}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}
                            </Accordion>
                        </Tab>
                        {meeting && meeting.settings.shareMeetingInvitePeople ? (
                            <Tab
                                eventKey='invite'
                                title={<FontAwesomeIcon icon={faUserPlus} size='lg' />}>
                                <Accordion defaultActiveKey={1}>
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={1}
                                                variant='link'>
                                                <FontAwesomeIcon icon={faPaperPlane} size='sm' />
                                                Invite By Email
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={1}>
                                            <div className='card-body'>
                                                <InviteParticipants meeting={this.props.meeting} />
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={2}
                                                variant='link'>
                                                <FontAwesomeIcon icon={faShareAlt} size='sm' />
                                                Share Meeting Link
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={2}>
                                            <div className='card-body'>
                                                <div className='row'>
                                                    <div className='col-md-12'>
                                                        <button
                                                            className='btn btn-success btn-sm mr-2'
                                                            onClick={generateGuestLink}
                                                            type='button'>
                                                            Generate & Copy Guest Link
                                                        </button>
                                                        {guestLink !== '' ? (
                                                            <>
                                                                <button
                                                                    className='btn btn-info btn-sm mr-2'
                                                                    onClick={copyLink}
                                                                    type='button'>
                                                                    Copy Link
                                                                </button>
                                                                <textarea
                                                                    className='form-control mt-3'
                                                                    readOnly={true}
                                                                    value={guestLink}
                                                                />
                                                            </>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                </Accordion>
                            </Tab>
                        ) : null}
                        <Tab
                            eventKey='lock'
                            title={<FontAwesomeIcon icon={faHandshake} size='lg' />}>
                            <Accordion>
                                {/* recording && meeting.settings.screenRecording */}
                                {meeting && meeting.settings.screenRecording ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={'recording'}
                                                variant='link'>
                                                Screen Recording
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={'recording'}>
                                            <div className='card-body radius-5'>
                                                {activeTab === 'lock' ? (
                                                    <div>
                                                        {typeof MediaRecorder !== 'undefined' ? (
                                                            <div>
                                                                <p
                                                                    style={{
                                                                        fontSize: '14px'
                                                                    }}>
                                                                    The screen recording will be
                                                                    processed by utilizing browser's
                                                                    share screen feature. So the
                                                                    permission should be granted.{' '}
                                                                    <strong>
                                                                        For better recording quality
                                                                        and capturing remote audio,
                                                                        one should be on speakers
                                                                        while initiating a recording
                                                                        session.
                                                                    </strong>
                                                                    <br />
                                                                    Status :
                                                                    {this.props.recorder !== null
                                                                        ? this.props.recorder.state
                                                                        : 'Not Started'}
                                                                </p>
                                                                {this.props.recorder === null ||
                                                                this.props.recorder.state !==
                                                                    'recording' ? (
                                                                    <button
                                                                        onClick={startRecording}
                                                                        className='btn btn-primary btn-sm'
                                                                        type='button'>
                                                                        Start Recording
                                                                    </button>
                                                                ) : null}

                                                                {this.props.recorder !== null &&
                                                                this.props.recorder.state ===
                                                                    'recording' ? (
                                                                    <button
                                                                        onClick={() => {
                                                                            stopRecording();
                                                                        }}
                                                                        className='btn btn-danger btn-sm'
                                                                        type='button'>
                                                                        Stop Recording
                                                                    </button>
                                                                ) : null}
                                                            </div>
                                                        ) : (
                                                            <p
                                                                style={{
                                                                    fontSize: '14px'
                                                                }}>
                                                                Your browser don't support screen
                                                                recording!
                                                            </p>
                                                        )}
                                                    </div>
                                                ) : null}
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}

                                {meeting && meeting.settings.manageVideoQuality ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={1}
                                                variant='link'>
                                                Manage Video Quality
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={1}>
                                            <div className='card-body'>
                                                <VideoQualitySlider />
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}

                                {meeting && meeting.settings.lockRoom ? (
                                    <div className='card'>
                                        <div className='card-header'>
                                            <Accordion.Toggle
                                                as={Button}
                                                eventKey={2}
                                                variant='link'>
                                                Lock Room
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey={2}>
                                            <div className='card-body radius-5'>
                                                <div className='list-group-item'>
                                                    <div className='row'>
                                                        <div className='col-md-9'>
                                                            <span>Meeting Room</span>
                                                        </div>
                                                        <div className='col-md-3'>
                                                            <div className='onoffswitch'>
                                                                <input
                                                                    checked={meeting.lockRoom}
                                                                    className='onoffswitch-checkbox'
                                                                    id='myonoffswitch'
                                                                    name='onoffswitch'
                                                                    onChange={toggleLock}
                                                                    tabIndex='0'
                                                                    type='checkbox'
                                                                    value={meeting.lockRoom}
                                                                />
                                                                <label
                                                                    className='onoffswitch-label'
                                                                    htmlFor='myonoffswitch'>
                                                                    <span className='onoffswitch-inner' />
                                                                    <span className='onoffswitch-switch' />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                ) : null}
                            </Accordion>
                        </Tab>
                        {meeting &&
                        meeting.settings.shareScreen &&
                        this.props.participants.length > 1 ? (
                            <Tab
                                eventKey='share'
                                title={<FontAwesomeIcon icon={faDesktop} size='lg' />}>
                                <div className='card'>
                                    <div className='list-group list-group-flush'>
                                        <div className='list-group-item'>
                                            <div className='row'>
                                                <div className='col-md-9'>
                                                    <span>Screen Share</span>
                                                </div>
                                                <div className='col-md-3'>
                                                    <div className='onoffswitch'>
                                                        <input
                                                            checked={this.props._screensharing}
                                                            className='onoffswitch-checkbox'
                                                            id='myonoffswitchs'
                                                            name='onoffswitchs'
                                                            onChange={
                                                                this.props.toggleScreensharing
                                                            }
                                                            tabIndex='0'
                                                            type='checkbox'
                                                        />
                                                        <label
                                                            className='onoffswitch-label'
                                                            htmlFor='myonoffswitchs'>
                                                            <span className='onoffswitch-inner' />
                                                            <span className='onoffswitch-switch' />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Tab>
                        ) : null}
                        {meeting && meeting.settings.chat ? (
                            <Tab
                                eventKey='new-chat'
                                title={<FontAwesomeIcon icon={faCommentAlt} size='lg' />}>
                                <div>
                                    <NewChat conference={conference} key='chat' />
                                </div>
                            </Tab>
                        ) : null}

                        <Tab
                            eventKey='youtube'
                            title={<FontAwesomeIcon icon={faYoutube} size='lg' />}>
                            <div className='card'>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <div className='live-stream-btn'>
                                            {/* && meeting.settings.shareYoutube */}
                                            {meeting ? (
                                                <VideoShareButton
                                                    key='sharevideo'
                                                    sharingVideo={this.props.sharingVideo}
                                                    showLabel={true}
                                                />
                                            ) : null}
                                            {meeting && meeting.settings.startLiveStream ? (
                                                <LiveStreamButton
                                                    key='livestreaming'
                                                    showLabel={true}
                                                />
                                            ) : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Tab>
                    </Tabs>
                </div>
            </div>
        ) : null;
    }
}
/**
 * Maps (parts of) the redux state to the React {@code Component} props.
 *
 * @param {Object} state - The redux state.
 * @returns {Object}
 */

function mapStateToProps(state): Object {
    const localVideo = getLocalVideoTrack(state['features/base/tracks']);
    const localParticipant = getLocalParticipant(state);
    const nonJoinedParticipantsList = state['features/toolbox'].nonJoinedParticipantsList;
    const waitingParticipantsList = state['features/toolbox'].waitingParticipantsList;
    const guestLink = state['features/toolbox'].guestLink; // console.clear();
    const user = state['features/base/settings'].user;

    const participants = state['features/base/participants'];

    return {
        meeting: getMeetingDetails(state),
        _screensharing: localVideo && localVideo.videoType === 'desktop',
        localParticipantId: localParticipant.id,
        nonJoinedParticipantsList,
        waitingParticipantsList,
        guestLink,
        participants,
        recorder: state['features/toolbox'].recorder,
        isHost: typeof user !== 'undefined' && user !== null ? user.isHost : false
    };
}

const mapDispatchToProps = {
    toggleScreensharing,
    sendInvitation,
    getNonJoinedParticipants,
    getWaitingParticipants,
    muteAllParticipants,
    showNotification,
    admitParticipant,
    rejectParticipant,
    generateGuestLink,
    lockMeetingRoom,
    updateRecorder,
    pollEvent,
    trackEvent
};
export default connect(mapStateToProps, mapDispatchToProps)(translate(SettingsSidebar));
