// @flow
// global interfaceConfig;

import React, { Component } from 'react';

import { translate } from '../../../base/i18n';
import { connect } from '../../../base/redux';
import { sendInvitation, inviteDone } from '../../actions';

type State = {
    inviteEmail: string;
    inviteName: string;
};

type Props = {
    sendInvitation: Function;
    inviteMessage: string;
    meeting: Object;
};

/**
 * Meeting info
 */
class InviteParticipants extends Component<Props, State> {
    /**
     * Component constructor.
     *
     * @param {*} props - Props.
     * @returns {void}.
     */
    constructor(props) {
        super(props);

        this.state = {
            inviteName: '',
            inviteEmail: ''
        };

        this.onInvite = this.onInvite.bind(this);
        this.onChangeInviteEmail = this.onChangeInviteEmail.bind(this);
        this.onChangeInviteName = this.onChangeInviteName.bind(this);
    }

    /**
     * Set invite name.
     *
     * @param {*} e - Event.
     * @returns {*}
     */
    onChangeInviteName({ target: { value } }) {
        this.setState({ inviteName: value });
    }

    /**
     * Set invite email.
     *
     * @param {*} e - Event.
     * @returns {*}
     */
    onChangeInviteEmail({ target: { value } }) {
        this.setState({ inviteEmail: value });
    }

    onInvite: () => void;

    /**
     * Invite someone.
     *
     * @param {*} e - Event.
     * @returns {void}
     */
    onInvite(e) {
        e.preventDefault();
        const { inviteName, inviteEmail } = this.state;

        this.props
            .sendInvitation(inviteName, inviteEmail, this.props.meeting.meetingId)
            .then(() => {
                this.setState({ inviteName: '', inviteEmail: '' });
            });
    }

    /**
     * Render.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { onInvite } = this;

        return (
            <form onSubmit={ onInvite }>
                <div className='form-row'>
                    <div className='form-group col-md-12'>
                        <input
                            className='form-control'
                            onChange={ this.onChangeInviteEmail }
                            placeholder='Enter Email'
                            type='email'
                            value={ this.state.inviteEmail } />
                    </div>
                    <div className='form-group col-md-12'>
                        <input
                            className='form-control'
                            onChange={ this.onChangeInviteName }
                            placeholder='Enter Name'
                            type='text'
                            value={ this.state.inviteName } />
                    </div>
                </div>
                <button
                    className='btn btn-primary'
                    disabled={ this.state.inviteEmail === '' || this.state.inviteName === '' }
                    type='submit'>
                    Send Invite
                </button>
            </form>
        );
    }
}

/**
 * Maps (parts of) the redux state to the React {@code Component} props.
 *
 * @param {Object} state - The redux state.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    const { inviteMessage } = state['features/toolbox'];

    return {
        inviteMessage
    };
}

const mapDispatchToProps = {
    sendInvitation,
    inviteDone
};

export default connect(mapStateToProps, mapDispatchToProps)(translate(InviteParticipants));
