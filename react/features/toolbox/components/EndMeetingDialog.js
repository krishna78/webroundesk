// @flow

import moment from 'moment';
import React, {Component} from 'react';
import type {Dispatch} from 'redux';
import {createToolbarEvent, sendAnalytics} from '../../analytics';
import {Dialog} from '../../base/dialog';
import {translate} from '../../base/i18n';
import {PARTICIPANT_ROLE, getLocalParticipant, kickParticipant} from '../../base/participants';
import {connect} from '../../base/redux';
import {showNotification, showErrorNotification} from '../../notifications/actions';
import {cleanUp, endMeeting} from '../../welcome/actions.web';
import {appNavigate} from '../../app/actions';
import {disconnect} from '../../base/connection';
import {trackEvent} from '../../toolbox/actions.web';

/**
 * The type of the React {@code Component} props of
 * {@link EndMeetingDialog}.
 */
type Props = {
    /**
     * Redux store dispatch function.
     */
    dispatch: Dispatch<any>;

    /**
     * Current encoding format.
     */
    encodingFormat: string;

    /**
     * Whether the local user is the moderator.
     */
    isModerator: boolean;

    /**
     * Whether local recording is engaged.
     */
    isEngaged: boolean;

    /**
     * The start time of the current local recording session.
     * Used to calculate the duration of recording.
     */
    recordingEngagedAt: Date;

    /**
     * Stats of all the participant.
     */
    stats: Object;

    /**
     * Invoked to obtain translated strings.
     */
    t: Function;
    meeting: Object;
};

/**
 * The type of the React {@code Component} state of
 * {@link EndMeetingDialog}.
 */
type State = {
    /**
     * The recording duration string to be displayed on the UI.
     */
    durationString: string;
};

/**
 * A React Component with the contents for a dialog that shows information about
 * local recording. For users with moderator rights, this is also the "control
 * panel" for starting/stopping local recording on all clients.
 *
 * @extends Component
 */
class EndMeetingDialog extends Component<Props, State> {
    /**
     * Saves a handle to the timer for UI updates,
     * so that it can be cancelled when the component unmounts.
     */
    _timer: ?IntervalID;

    /**
     * Initializes a new {@code EndMeetingDialog} instance.
     *
     * @param {Props} props - The React {@code Component} props to initialize
     * the new {@code EndMeetingDialog} instance with.
     */
    constructor(props: Props) {
        super(props);
        this.endMeetingForAll = this.endMeetingForAll.bind(this);
        this.leaveMeeting = this.leaveMeeting.bind(this);
        this.kickoutAll = this.kickoutAll.bind(this);
    }

    /**
     * Implements React's {@link Component#componentDidMount()}.
     *
     * @returns {void}
     */
    componentDidMount() {}

    /**
     * Implements React's {@link Component#componentWillUnmount()}.
     *
     * @returns {void}
     */
    componentWillUnmount() {}

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const {isModerator, t} = this.props;

        return (
            <Dialog cancelKey={'dialog.close'} className='rd-modal' submitDisabled={true}>
                <div className='audio-recorder'>
                    <p>
                        End meeting for everyone? this will kickout all participants from the
                        meeting!
                    </p>
                    <button
                        className='btn btn-danger mr-4'
                        type='button'
                        onClick={this.endMeetingForAll}>
                        Yes, End Meeting
                    </button>
                    <button className='btn btn-success' type='button' onClick={this.leaveMeeting}>
                        No, Leave Meeting
                    </button>
                </div>
            </Dialog>
        );
    }

    endMeetingForAll() {
        // end meeting for all
        this.props.dispatch(trackEvent('END_MEETING'));
        sendAnalytics(createToolbarEvent('hangup'));
        if (this.props.recorder !== null && this.props.recorder.state === 'recording') {
            this.props.recorder.stop();
            if (
                this.props.audioRecorder !== null &&
                this.props.audioRecorder.state === 'recording'
            ) {
                this.props.audioRecorder.stop();
            }
            setTimeout(() => {
                this.kickoutAll();
            }, 5000);
        } else {
            if (
                this.props.audioRecorder !== null &&
                this.props.audioRecorder.state === 'recording'
            ) {
                this.props.audioRecorder.stop();
            }
            this.kickoutAll();
        }
    }

    kickoutAll() {
        this.props.participants.forEach(participant => {
            if (participant.id !== this.props.localId) {
                this.props.dispatch(kickParticipant(participant.id));
            }
        });

        this.props.dispatch(
            endMeeting(
                this.props.data.meetingId,
                this.props.data,
                navigator.product,
                appNavigate,
                disconnect,
                cleanUp
            )
        );
    }

    leaveMeeting() {
        console.clear();
        this.props.dispatch(trackEvent('LEAVE_MEETING'));
        sendAnalytics(createToolbarEvent('hangup'));
        if (this.props.recorder !== null && this.props.recorder.state === 'recording') {
            this.props.recorder.stop();
            if (
                this.props.audioRecorder !== null &&
                this.props.audioRecorder.state === 'recording'
            ) {
                this.props.audioRecorder.stop();
            }
            setTimeout(() => {
                this.props.dispatch(
                    endMeeting(
                        this.props.data.meetingId,
                        this.props.data,
                        navigator.product,
                        appNavigate,
                        disconnect,
                        cleanUp
                    )
                );
            }, 5000);
        } else {
            if (
                this.props.audioRecorder !== null &&
                this.props.audioRecorder.state === 'recording'
            ) {
                this.props.audioRecorder.stop();
            }

            this.props.dispatch(
                endMeeting(
                    this.props.data.meetingId,
                    this.props.data,
                    navigator.product,
                    appNavigate,
                    disconnect,
                    cleanUp
                )
            );
        }
    }
}

/**
 * Maps (parts of) the Redux state to the associated props for the
 * {@code EndMeetingDialog} component.
 *
 * @param {Object } state - The Redux state.
 * @private
 * @returns {{
 *     encodingFormat: string,
 *     isModerator: boolean,
 *     isEngaged: boolean,
 *     recordingEngagedAt: Date,
 *     stats: Object
 * } }
 */
function _mapStateToProps(state) {
    const {encodingFormat, isEngaged, recordingEngagedAt, stats} = state[
        'features/local-recording'
    ];
    const user = state['features/base/settings'].user || state['features/welcome'].user;
    const isModerator = typeof user !== 'undefined' && user !== null ? user.isHost : false; //getLocalParticipant(state).role === PARTICIPANT_ROLE.MODERATOR;
    const conference = state['features/base/conference'];

    let data = {};

    if (typeof conference !== 'undefined' && typeof conference.conference !== 'undefined') {
        data = {
            user,
            conferenceTimeStamp: conference.conferenceTimestamp,
            meetingId: conference.room,
            speakerStats: conference.conference.speakerStatsCollector.stats.users,
            analytics: state['features/analytics'],
            link: state['features/base/connection'].locationURL,
            participants: state['features/base/participants'],
            chat: state['features/chat'].messages,
            video: state['features/shared-video'],
            lobby: state['features/toolbox'].waitingParticipantsList,
            nonJoined: state['features/toolbox'].nonJoinedParticipantsList,
            endedOn: new Date()
        };
    }

    return {
        encodingFormat,
        isModerator,
        isEngaged,
        recordingEngagedAt,
        stats,
        participants: state['features/base/participants'],
        meeting: state['features/base/settings'].meeting,
        audioRecorder: state['features/toolbox'].audioRecorder,
        data,
        isHost: typeof user !== 'undefined' ? user.isHost : false,
        recorder: state['features/toolbox'].recorder,
        localId: getLocalParticipant(state).id
    };
}

export default translate(connect(_mapStateToProps)(EndMeetingDialog));
