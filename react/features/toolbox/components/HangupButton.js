// @flow

import _ from 'lodash';

import {createToolbarEvent, sendAnalytics} from '../../analytics';
import {appNavigate} from '../../app/actions';
import {disconnect} from '../../base/connection';
import {translate} from '../../base/i18n';
import {connect} from '../../base/redux';
import {openDialog, toggleDialog} from '../../base/dialog';
import {AbstractHangupButton} from '../../base/toolbox/components';
import type {AbstractButtonProps} from '../../base/toolbox/components';
import {cleanUp, endMeeting} from '../../welcome/actions';
import {parse, stringify} from 'flatted';
import EndMeetingDialog from './EndMeetingDialog';
import {trackEvent} from '../actions.web';

/**
 * The type of the React {@code Component} props of {@link HangupButton}.
 */
type Props = AbstractButtonProps & {
    /**
     * The redux {@code dispatch} function.
     */
    dispatch: Function;
    recorder: any;
    audioRecorder: any;
    data: any;
};

/**
 * Component that renders a toolbar button for leaving the current conference.
 *
 * @extends AbstractHangupButton
 */
class HangupButton extends AbstractHangupButton<Props, *> {
    _hangup: Function;

    accessibilityLabel = 'toolbar.accessibilityLabel.hangup';
    label = 'toolbar.hangup';
    tooltip = 'toolbar.hangup';

    /**
     * Initializes a new HangupButton instance.
     *
     * @param {Props} props - The read-only properties with which the new
     * instance is to be initialized.
     */
    constructor(props: Props) {
        super(props);

        // this._hangup = _.once(() => {
        //     //
        //     if (props.isHost) {
        //         this.props.dispatch(openDialog(EndMeetingDialog));
        //     } else {
        //         this.props.dispatch(trackEvent('LEAVE_MEETING'));
        //         console.clear();
        //         sendAnalytics(createToolbarEvent('hangup'));
        //         if (this.props.recorder !== null && this.props.recorder.state === 'recording') {
        //             this.props.recorder.stop();
        //             if (
        //                 this.props.audioRecorder !== null &&
        //                 this.props.audioRecorder.state === 'recording'
        //             ) {
        //                 this.props.audioRecorder.stop();
        //             }
        //             setTimeout(() => {
        //                 this.props.dispatch(
        //                     endMeeting(
        //                         this.props.data.meetingId,
        //                         this.props.data,
        //                         navigator.product,
        //                         appNavigate,
        //                         disconnect,
        //                         cleanUp
        //                     )
        //                 );
        //             }, 5000);
        //         } else {
        //             if (
        //                 this.props.audioRecorder !== null &&
        //                 this.props.audioRecorder.state === 'recording'
        //             ) {
        //                 this.props.audioRecorder.stop();
        //             }

        //             this.props.dispatch(
        //                 endMeeting(
        //                     this.props.data.meetingId,
        //                     this.props.data,
        //                     navigator.product,
        //                     appNavigate,
        //                     disconnect,
        //                     cleanUp
        //                 )
        //             );
        //         }
        //     }
        // });

        this._hangup = () => {
            //
            if (props.isHost) {
                this.props.dispatch(openDialog(EndMeetingDialog));
            } else {
                this.props.dispatch(trackEvent('LEAVE_MEETING'));
                console.clear();
                sendAnalytics(createToolbarEvent('hangup'));
                if (this.props.recorder !== null && this.props.recorder.state === 'recording') {
                    this.props.recorder.stop();
                    if (
                        this.props.audioRecorder !== null &&
                        this.props.audioRecorder.state === 'recording'
                    ) {
                        this.props.audioRecorder.stop();
                    }
                    setTimeout(() => {
                        this.props.dispatch(
                            endMeeting(
                                this.props.data.meetingId,
                                this.props.data,
                                navigator.product,
                                appNavigate,
                                disconnect,
                                cleanUp
                            )
                        );
                    }, 5000);
                } else {
                    if (
                        this.props.audioRecorder !== null &&
                        this.props.audioRecorder.state === 'recording'
                    ) {
                        this.props.audioRecorder.stop();
                    }

                    this.props.dispatch(
                        endMeeting(
                            this.props.data.meetingId,
                            this.props.data,
                            navigator.product,
                            appNavigate,
                            disconnect,
                            cleanUp
                        )
                    );
                }
            }
        };
    }

    /**
     * Helper function to perform the actual hangup action.
     *
     * @override
     * @protected
     * @returns {void}
     */
    _doHangup() {
        this._hangup();
    }
}

// if (typeof JSON.decycle !== "function") {
//     JSON.decycle = function decycle(object, replacer) {
//         "use strict";

//         var objects = new WeakMap();     // object to path mappings

//         return (function derez(value, path) {
//             var old_path;   // The path of an earlier occurance of value
//             var nu;         // The new object or arra

//             if (replacer !== undefined) {
//                 value = replacer(value);
//             }
//             if (
//                 typeof value === "object"
//                 && value !== null
//                 && !(value instanceof Boolean)
//                 && !(value instanceof Date)
//                 && !(value instanceof Number)
//                 && !(value instanceof RegExp)
//                 && !(value instanceof String)
//             ) {

//                 old_path = objects.get(value);
//                 if (old_path !== undefined) {
//                     return { $ref: old_path };
//                 }
//                 objects.set(value, path);
//                 if (Array.isArray(value)) {
//                     nu = [];
//                     value.forEach(function(element, i) {
//                         nu[i] = derez(element, path + "[" + i + "]");
//                     });
//                 } else {
//                     nu = {};
//                     Object.keys(value).forEach(function(name) {
//                         nu[name] = derez(
//                             value[name],
//                             path + "[" + JSON.stringify(name) + "]"
//                         );
//                     });
//                 }
//                 return nu;
//             }
//             return value;
//         }(object, "$"));
//     };
// }

// if (typeof JSON.retrocycle !== "function") {
//     JSON.retrocycle = function retrocycle($) {
//         "use strict";

//         var px = /^\$(?:\[(?:\d+|"(?:[^\\"\u0000-\u001f]|\\(?:[\\"\/bfnrt]|u[0-9a-zA-Z]{4}))*")\])*$/;

//         (function rez(value) {

//             if (value && typeof value === "object") {
//                 if (Array.isArray(value)) {
//                     value.forEach(function(element, i) {
//                         if (typeof element === "object" && element !== null) {
//                             var path = element.$ref;
//                             if (typeof path === "string" && px.test(path)) {
//                                 value[i] = eval(path);
//                             } else {
//                                 rez(element);
//                             }
//                         }
//                     });
//                 } else {
//                     Object.keys(value).forEach(function(name) {
//                         var item = value[name];
//                         if (typeof item === "object" && item !== null) {
//                             var path = item.$ref;
//                             if (typeof path === "string" && px.test(path)) {
//                                 value[name] = eval(path);
//                             } else {
//                                 rez(item);
//                             }
//                         }
//                     });
//                 }
//             }
//         }($));
//         return $;
//     };
// }

function _mapStateToProps(state: Object) {
    const conference = state['features/base/conference'];
    const user = state['features/base/settings'].user || state['features/welcome'].user;

    let data = {};

    if (typeof conference !== 'undefined' && typeof conference.conference !== 'undefined') {
        data = {
            user,
            conferenceTimeStamp: conference.conferenceTimestamp,
            meetingId: conference.room,
            speakerStats: conference.conference.speakerStatsCollector.stats.users,
            analytics: state['features/analytics'],
            link: state['features/base/connection'].locationURL,
            participants: state['features/base/participants'],
            chat: state['features/chat'].messages,
            video: state['features/shared-video'],
            lobby: state['features/toolbox'].waitingParticipantsList,
            nonJoined: state['features/toolbox'].nonJoinedParticipantsList,
            endedOn: new Date()
        };
    }

    return {
        data,
        isHost: typeof user !== 'undefined' ? user.isHost : false,
        recorder: state['features/toolbox'].recorder,
        audioRecorder: state['features/toolbox'].audioRecorder
    };
}

export default translate(connect(_mapStateToProps)(HangupButton));
