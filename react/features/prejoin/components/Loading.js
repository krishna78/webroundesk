// @flow

// import InlineDialog from '@atlaskit/inline-dialog';
import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';

import { getRoomName } from '../../base/conference';
import { translate } from '../../base/i18n';
// import { Icon, IconPhone, IconVolumeOff } from '../../base/icons';
import { isVideoMutedByUser } from '../../base/media';
import { PreMeetingScreen } from '../../base/premeeting';
import { connect } from '../../base/redux';
import {
    getDisplayName,
    updateSettings,
    getMeetingDetails,
    getUserDetails,
    getAppToken,
    getLockStatus
} from '../../base/settings';
import { getLocalJitsiVideoTrack } from '../../base/tracks';
import { fetchMeeting } from '../../welcome/actions';
import {
    joinConference as joinConferenceAction,
    joinConferenceWithoutAudio as joinConferenceWithoutAudioAction,
    setSkipPrejoin as setSkipPrejoinAction,
    setJoinByPhoneDialogVisiblity as setJoinByPhoneDialogVisiblityAction,
    loginAsHost,
    loginAsParticipant,
    confirmPassword,
    unlock,
    lock
} from '../actions';
import {
    isDeviceStatusVisible,
    isDisplayNameRequired,
    isJoinByPhoneButtonVisible,
    isJoinByPhoneDialogVisible
} from '../functions';

// import JoinByPhoneDialog from './dialogs/JoinByPhoneDialog';
// import { ActionButton, InputField, PreMeetingScreen } from '../../base/premeeting';
// import { Icon, IconPhone, IconVolumeOff } from '../../base/icons';
import DeviceStatus from './preview/DeviceStatus';

type Props = {
    /**
     * Flag signaling if the device status is visible or not.
     */
    deviceStatusVisible: boolean;

    /**
     * If join by phone button should be visible.
     */
    hasJoinByPhoneButton: boolean;

    /**
     * If join button is disabled or not.
     */
    joinButtonDisabled: boolean,

    /**
     * Joins the current meeting.
     */
    joinConference: Function;

    /**
     * Joins the current meeting without audio.
     */
    joinConferenceWithoutAudio: Function;

    /**
     * The name of the user that is about to join.
     */
    name: string;

    /**
     * Updates settings.
     */
    updateSettings: Function;

    /**
     * The name of the meeting that is about to be joined.
     */
    roomName: string;

    /**
     * Sets visibility of the prejoin page for the next sessions.
     */
    setSkipPrejoin: Function;

    /**
     * Sets visibility of the 'JoinByPhoneDialog'.
     */
    setJoinByPhoneDialogVisiblity: Function;

    /**
     * Flag signaling the visibility of camera preview.
     */
    showCameraPreview: boolean;

    /**
     * If 'JoinByPhoneDialog' is visible or not.
     */
    showDialog: boolean;

    /**
     * Used for translation.
     */
    t: Function;

    /**
     * The JitsiLocalTrack to display.
     */
    videoTrack: ?Object;
    meeting: Object;
    loginAsHost: Function;
    user: Object;
    token: any;
    fetchMeeting: Function;
    loginAsParticipant: Function;
};

type State = {
    /**
     * Flag controlling the visibility of the 'join by phone' buttons.
     */
    showJoinByPhoneButtons: boolean;
    loginRequired: boolean;
    password: string;
    email: string;
};

/**
 * This component is displayed before joining a meeting.
 */
class Loading extends Component<Props, State> {
    /**
     * Initializes a new {@code Prejoin} instance.
     *
     * @inheritdoc
     */
    constructor(props) {
        super(props);
    }

    /**
     * Run after component mounted.
     *
     * @returns {void}.
     */
    componentDidMount() {
        //
    }


    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {

        return (
            <>
                <div className='loader'>
                    <div className='overlay show' />
                    <div className='spanner show'>
                        <div className='loader' />
                        <p>Processing...</p>
                    </div>
                </div>
            </>
        );
    }
}

/**
 * Maps (parts of) the redux state to the React {@code Component} props.
 *
 * @param {Object} state - The redux state.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    const name = getDisplayName(state);
    const joinButtonDisabled = isDisplayNameRequired(state) && !name;

    return {
        joinButtonDisabled,
        name,
        deviceStatusVisible: isDeviceStatusVisible(state),
        roomName: getRoomName(state),
        showDialog: isJoinByPhoneDialogVisible(state),
        hasJoinByPhoneButton: isJoinByPhoneButtonVisible(state),
        showCameraPreview: !isVideoMutedByUser(state),
        videoTrack: getLocalJitsiVideoTrack(state),
        meeting: getMeetingDetails(state),
        user: getUserDetails(state),
        token: getAppToken(state),
        locked: getLockStatus(state)
    };
}

const mapDispatchToProps = {
    joinConferenceWithoutAudio: joinConferenceWithoutAudioAction,
    joinConference: joinConferenceAction,
    setJoinByPhoneDialogVisiblity: setJoinByPhoneDialogVisiblityAction,
    setSkipPrejoin: setSkipPrejoinAction,
    updateSettings,
    loginAsHost,
    loginAsParticipant,
    fetchMeeting,
    confirmPassword,
    unlock,
    lock
};

export default connect(mapStateToProps, mapDispatchToProps)(translate(Loading));
