// @flow

// import InlineDialog from '@atlaskit/inline-dialog';
import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';

import {getRoomName} from '../../base/conference';
import {translate} from '../../base/i18n';
// import { Icon, IconPhone, IconVolumeOff } from '../../base/icons';
import {isVideoMutedByUser} from '../../base/media';
import {PreMeetingScreen} from '../../base/premeeting';
import {connect} from '../../base/redux';
import {
    getDisplayName,
    updateSettings,
    getMeetingDetails,
    getUserDetails,
    getAppToken,
    getLockStatus
} from '../../base/settings';
import {getLocalJitsiVideoTrack} from '../../base/tracks';
import {fetchMeeting} from '../../welcome/actions.web';
import {
    joinConference as joinConferenceAction,
    joinConferenceWithoutAudio as joinConferenceWithoutAudioAction,
    setSkipPrejoin as setSkipPrejoinAction,
    setJoinByPhoneDialogVisiblity as setJoinByPhoneDialogVisiblityAction,
    loginAsHost,
    loginAsParticipant,
    confirmPassword,
    unlock,
    lock,
    setNext,
    setWait
} from '../actions';
import {
    isDeviceStatusVisible,
    isDisplayNameRequired,
    isJoinByPhoneButtonVisible,
    isJoinByPhoneDialogVisible
} from '../functions';
import Loading from './Loading';

// import JoinByPhoneDialog from './dialogs/JoinByPhoneDialog';
// import { ActionButton, InputField, PreMeetingScreen } from '../../base/premeeting';
// import { Icon, IconPhone, IconVolumeOff } from '../../base/icons';
import DeviceStatus from './preview/DeviceStatus';

type Props = {
    /**
     * Flag signaling if the device status is visible or not.
     */
    deviceStatusVisible: boolean;

    /**
     * If join by phone button should be visible.
     */
    hasJoinByPhoneButton: boolean;

    /**
     * If join button is disabled or not.
     */
    joinButtonDisabled: boolean;

    /**
     * Joins the current meeting.
     */
    joinConference: Function;

    /**
     * Joins the current meeting without audio.
     */
    joinConferenceWithoutAudio: Function;

    /**
     * The name of the user that is about to join.
     */
    name: string;

    /**
     * Updates settings.
     */
    updateSettings: Function;

    /**
     * The name of the meeting that is about to be joined.
     */
    roomName: string;

    /**
     * Sets visibility of the prejoin page for the next sessions.
     */
    setSkipPrejoin: Function;

    /**
     * Sets visibility of the 'JoinByPhoneDialog'.
     */
    setJoinByPhoneDialogVisiblity: Function;

    /**
     * Flag signaling the visibility of camera preview.
     */
    showCameraPreview: boolean;

    /**
     * If 'JoinByPhoneDialog' is visible or not.
     */
    showDialog: boolean;

    /**
     * Used for translation.
     */
    t: Function;

    /**
     * The JitsiLocalTrack to display.
     */
    videoTrack: ?Object;
    meeting: Object;
    loginAsHost: Function;
    user: Object;
    token: any;
    fetchMeeting: Function;
    loginAsParticipant: Function;
    setNext: Function;
    confirmPassword: Function;
    unlock: Function;
    setWait: Function;
    locked: boolean;
    wait: boolean;
    next: boolean;
};

type State = {
    /**
     * Flag controlling the visibility of the 'join by phone' buttons.
     */
    showJoinByPhoneButtons: boolean;
    loginRequired: boolean;
    password: string;
    email: string;
    wait: boolean;
    meetingPassword: string;
    showSettings: boolean;
};

/**
 * This component is displayed before joining a meeting.
 */
class Prejoin extends Component<Props, State> {
    /**
     * Initializes a new {@code Prejoin} instance.
     *
     * @inheritdoc
     */
    constructor(props) {
        super(props);
        //console.error('from constructor');

        this.state = {
            showJoinByPhoneButtons: false,
            loginRequired: false,
            password: '',
            email: '',
            showSettings: false,
            meetingPassword: '',
            wait: true
        };

        this._closeDialog = this._closeDialog.bind(this);
        this._showDialog = this._showDialog.bind(this);
        this._onCheckboxChange = this._onCheckboxChange.bind(this);
        this._onDropdownClose = this._onDropdownClose.bind(this);
        this._onOptionsClick = this._onOptionsClick.bind(this);
        this._setName = this._setName.bind(this);
        this.onJoinTypeChanged = this.onJoinTypeChanged.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this._onNext = this._onNext.bind(this);
        this.proceedToSettings = this.proceedToSettings.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
    }

    proceedToSettings: () => void;
    passwordChange: () => void;

    /**
     * Run after component mounted.
     *
     * @returns {void}.
     */
    componentDidMount() {
        // this.props.lock();
        // const { meeting, token, roomName } = this.props;
        // if (meeting === null || !meeting.roomId) {
        //     this.props.fetchMeeting(roomName, token, () => { });
        // }
    }

    proceedToSettings(e) {
        e.preventDefault();
        // this.props.setNext(true);

        const {meeting} = this.props;
        if (meeting.passwordProtected) {
            this.props.confirmPassword(meeting.meetingId, this.state.meetingPassword);
        } else {
            this.props.unlock();
        }

        // this.setState({ showSettings: true });
    }

    onLogin: () => void;

    /**
     * Host login event.
     *
     * @param {*} e - Event.
     * @returns {void}.
     */
    onLogin(e) {
        e.preventDefault();
        this.handleClose();
        this.props.loginAsHost(this.state.email, this.state.password, this.props.meeting);
    }

    handleClose: () => void;

    /**
     * Modal close event.
     *
     * @returns {void}.
     */
    handleClose() {
        this.setState({
            loginRequired: false
        });
    }

    onEmailChange: () => void;

    /**
     * Email change event.
     *
     * @param {Object} e - Event.
     * @returns {void}.
     */
    onEmailChange(e) {
        this.setState({email: e.target.value});
    }

    onPasswordChange: () => void;

    /**
     * Password change event.
     *
     * @param {Object} e - Event.
     * @returns {void}.
     */
    onPasswordChange(e) {
        this.setState({password: e.target.value});
    }

    /**
     * Password change event.
     *
     * @param {Object} e - Event.
     * @returns {void}.
     */
    passwordChange(e) {
        this.setState({meetingPassword: e.target.value});
    }

    _onCheckboxChange: () => void;

    /**
     * Handler for the checkbox.
     *
     * @param {Object} e - The synthetic event.
     * @returns {void}
     */
    _onCheckboxChange(e) {
        this.props.setSkipPrejoin(e.target.checked);
    }

    _onDropdownClose: () => void;

    /**
     * Closes the dropdown.
     *
     * @returns {void}
     */
    _onDropdownClose() {
        this.setState({
            showJoinByPhoneButtons: false
        });
    }

    _onOptionsClick: () => void;

    /**
     * Displays the join by phone buttons dropdown.
     *
     * @param {Object} e - The synthetic event.
     * @returns {void}
     */
    _onOptionsClick(e) {
        e.stopPropagation();

        this.setState({
            showJoinByPhoneButtons: !this.state.showJoinByPhoneButtons
        });
    }

    onJoinTypeChanged: () => void;

    /**
     * Login type change event.
     *
     * @returns {void}.
     */
    onJoinTypeChanged() {
        this.setState({loginRequired: true});
    }

    _setName: () => void;

    /**
     * Sets the guest participant name.
     *
     * @param {string} displayName - Participant name.
     * @returns {void}
     */
    _setName({target: {value}}) {
        this.props.updateSettings({
            displayName: value
        });
    }

    _closeDialog: () => void;

    /**
     * Closes the join by phone dialog.
     *
     * @returns {undefined}
     */
    _closeDialog() {
        this.props.setJoinByPhoneDialogVisiblity(false);
    }

    _onNext: () => void;

    /**
     * Next.
     *
     * @returns {undefined}
     */
    _onNext() {
        const {user, joinConference, meeting, name} = this.props;
        if (meeting !== null) {
            this.props.setNext(true);

            if (meeting.isStarted === false) {
                this.props.setWait(true);
            } else {
                this.props.setWait(false);

                if (typeof user !== 'undefined' && user !== null && user.code && user.code !== '') {
                    if (!meeting.lockRoom || meeting.userType === 'host') {
                        joinConference();
                    } else if (meeting.userType !== 'host' && meeting.lockRoom) {
                        this.props.loginAsParticipant(name, meeting, user);
                        //this.props.unlock();
                    }
                } else {
                    this.props.loginAsParticipant(name, meeting);
                }
            }
        }
    }

    _showDialog: () => void;

    /**
     * Displays the dialog for joining a meeting by phone.
     *
     * @returns {undefined}
     */
    _showDialog() {
        this.props.setJoinByPhoneDialogVisiblity(true);
        this._onDropdownClose();
    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const {name, showCameraPreview, t, videoTrack, meeting, user} = this.props;

        const {
            _setName,
            onJoinTypeChanged,
            handleClose,
            onLogin,
            onEmailChange,
            onPasswordChange,
            passwordChange,
            _onNext
        } = this;
        const {loginRequired, email, password} = this.state;

        return (
            <>
                <Modal backdrop='static' keyboard={false} onHide={handleClose} show={loginRequired}>
                    <Modal.Header closeButton={true}>
                        <Modal.Title>Login</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={onLogin}>
                            <div className='form-group'>
                                <label htmlFor='email'>Email</label>
                                <input
                                    className='form-control'
                                    id='email'
                                    onChange={onEmailChange}
                                    type='email'
                                    value={email}
                                />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='password'>Password</label>
                                <input
                                    className='form-control'
                                    id='password'
                                    onChange={onPasswordChange}
                                    type='password'
                                    value={password}
                                />
                            </div>
                            <button
                                className='btn btn-primary float-right'
                                disabled={email === '' || password === ''}
                                type='submit'>
                                Login
                            </button>
                        </form>
                    </Modal.Body>
                </Modal>
                <PreMeetingScreen
                    footer=''
                    meeting={meeting}
                    name={name}
                    nameChange={_setName}
                    onLogin={onJoinTypeChanged}
                    loginRequired={this.state.loginRequired}
                    onNext={_onNext}
                    title={t('prejoin.joinMeeting')}
                    user={user}
                    passwordChange={passwordChange}
                    isUnLocked={this.props.locked}
                    showSettings={this.state.showSettings}
                    proceedToSettings={this.proceedToSettings}
                    videoMuted={!showCameraPreview}
                    videoTrack={videoTrack}
                    wait={this.props.wait}
                    next={this.props.next}>
                    {/* <div className='prejoin-input-area-container'>
                    <div className='prejoin-input-area'>
                        <InputField
                            onChange={ _setName }
                            onSubmit={ joinConference }
                            placeHolder={ t('dialog.enterDisplayName') }
                            value={ name } />

                        <div className='prejoin-preview-dropdown-container'>
                            <InlineDialog
                                content={ <div className='prejoin-preview-dropdown-btns'>
                                    <div
                                        className='prejoin-preview-dropdown-btn'
                                        onClick={ joinConferenceWithoutAudio }>
                                        <Icon
                                            className='prejoin-preview-dropdown-icon'
                                            size={ 24 }
                                            src={ IconVolumeOff } />
                                        { t('prejoin.joinWithoutAudio') }
                                    </div>
                                    { hasJoinByPhoneButton && <div
                                        className='prejoin-preview-dropdown-btn'
                                        onClick={ _showDialog }>
                                        <Icon
                                            className='prejoin-preview-dropdown-icon'
                                            size={ 24 }
                                            src={ IconPhone } />
                                        { t('prejoin.joinAudioByPhone') }
                                    </div> }
                                </div> }
                                isOpen={ showJoinByPhoneButtons }
                                onClose={ _onDropdownClose }>
                                <ActionButton
                                    disabled = { joinButtonDisabled }
                                    hasOptions = { true }
                                    onClick = { joinConference }
                                    onOptionsClick = { _onOptionsClick }
                                    type = 'primary'>
                                    { t('prejoin.joinMeeting') }
                                </ActionButton>
                            </InlineDialog>
                        </div>
                    </div>

                    <div className='prejoin-checkbox-container'>
                        <input
                            className='prejoin-checkbox'
                            onChange={ _onCheckboxChange }
                            type='checkbox' />
                        <span>{ t('prejoin.doNotShow') }</span>
                    </div>
                </div>
                { showDialog && (
                    <JoinByPhoneDialog
                        joinConferenceWithoutAudio={ joinConferenceWithoutAudio }
                        onClose={ _closeDialog } />
                ) } */}
                </PreMeetingScreen>
            </>
        );
    }

    /**
     * Renders the screen footer if any.
     *
     * @returns {React$Element}
     */
    _renderFooter() {
        return this.props.deviceStatusVisible && <DeviceStatus />;
    }
}

/**
 * Maps (parts of) the redux state to the React {@code Component} props.
 *
 * @param {Object} state - The redux state.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    //const user = state['features/base/settings'].user || state['features/welcome'].user;
    let name = getDisplayName(state);
    // if (typeof user !== 'undefined' && user !== null && user.name !== null) {
    //     name = user.name;
    // }

    // if (name === '' || name === null) {
    //     name = 'Fellow Roundesker';
    // }
    const joinButtonDisabled = isDisplayNameRequired(state) && !name;

    return {
        joinButtonDisabled,
        name,
        deviceStatusVisible: isDeviceStatusVisible(state),
        roomName: getRoomName(state),
        showDialog: isJoinByPhoneDialogVisible(state),
        hasJoinByPhoneButton: isJoinByPhoneButtonVisible(state),
        showCameraPreview: !isVideoMutedByUser(state),
        videoTrack: getLocalJitsiVideoTrack(state),
        meeting: getMeetingDetails(state),
        user: getUserDetails(state),
        token: getAppToken(state),
        locked: getLockStatus(state),
        wait: state['features/prejoin'].wait,
        next: state['features/prejoin'].next
    };
}

const mapDispatchToProps = {
    joinConferenceWithoutAudio: joinConferenceWithoutAudioAction,
    joinConference: joinConferenceAction,
    setJoinByPhoneDialogVisiblity: setJoinByPhoneDialogVisiblityAction,
    setSkipPrejoin: setSkipPrejoinAction,
    updateSettings,
    loginAsHost,
    loginAsParticipant,
    fetchMeeting,
    confirmPassword,
    unlock,
    lock,
    setNext,
    setWait
};

export default connect(mapStateToProps, mapDispatchToProps)(translate(Prejoin));
