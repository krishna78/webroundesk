// @flow
// global interfaceConfig;
import React, { Component } from 'react';

import { setAudioOnly } from '../../base/audio-only';
import { translate } from '../../base/i18n';
import { setVideoMuted } from '../../base/media';
import { connect } from '../../base/redux';
import { getLocalVideoType, isLocalVideoTrackMuted } from '../../base/tracks';
import { toggleVideoSettings, SettingsDialog } from '../../settings';
import VideoSettingsButton from '../../toolbox/components/web/VideoSettingsButton';
import { joinConference as joinConferenceAction } from '../actions';

declare var APP: Object;

type State = {};

type Props = {
    joinConference: Function;
    _videoMuted: boolean;
    videoMuted: boolean;
    onContinue: Function;
};

/**
 * Meeting info
 */
class Settings extends Component<Props, State> {
    /**
     * Component constructor.
     *
     * @param {*} props - Props.
     * @returns {void}.
     */
    constructor(props) {
        super(props);

        this.state = {
            cameraMuted: this.props._videoMuted
        };
    }

    /**
     * Render.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { onContinue, videoMuted } = this.props;

        return (
            <>
                <div className='settings-page'>
                    <div className='fxt-form'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <div>
                                    <div className='camera'>
                                        <label>Mute/Unmute Camera</label>
                                        <VideoSettingsButton
                                            key='vsb'
                                            visible={ true } />
                                    </div>
                                </div>
                            </div>
                            <SettingsDialog
                                onContinue={ onContinue }
                                videoMuted={ videoMuted } />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

/**
 * Map state to props.
 *
 * @param {*} state - State.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    const { enabled: audioOnly } = state['features/base/audio-only'];
    const tracks = state['features/base/tracks'];
    const _videoMuted = isLocalVideoTrackMuted(tracks);

    return {
        _videoMuted,
        _audioOnly: Boolean(audioOnly),
        _videoMediaType: getLocalVideoType(tracks),
        tracks
    };
}

const mapDispatchToProps = {
    joinConference: joinConferenceAction,
    setAudioOnly,
    setVideoMuted,
    toggleVideoSettings
};

export default connect(mapStateToProps, mapDispatchToProps)(translate(Settings));
