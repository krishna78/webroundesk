// @flow

/**
 * The default server URL to open if no other was specified.
 */
export const DEFAULT_SERVER_URL = 'https://uat.roundesk.io';

export const DEFAULT_API_URL = 'https://test.roundesk.io/api';
