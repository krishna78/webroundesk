// @flow

import React, { Component } from 'react';

import { connect } from '../../../redux';


export type Props = {
    onLogin: Function;
};

class WaitHost extends Component<Props> {

    /**
     * Implements {@code Component#render}.
     *
     * @inheritdoc
     */
    render() {
        return (<>
            <h4 className='text-center text-danger pt-3'>
                Meeting not yet started, wait for host to join.
            </h4>
            <div className='form-group mt-4'>
                <div className='fxt-transformY-50 fxt-transition-delay-4'>
                    <button
                        className='fxt-btn-fill'
                        onClick={ this.props.onLogin }
                        type='button'>
                        Join as Host
                    </button>
                </div>
            </div>
            <div className='col-md-12 text-center test fxt-transformY-50'>
                <h5 className='h55'>OR</h5>
            </div>
            <div className='form-group mt-4'>
                <div className='fxt-transformY-50 fxt-transition-delay-4'>
                    <a
                        className='fxt-btn-fill'
                        href={ `${location.protocol}//${location.host}` }>
                        Join another Meeting
                    </a>
                </div>
            </div>
        </>);
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @param {Props} ownProps - The own props of the component.
 * @returns {Props}
 */
function _mapStateToProps(state, ownProps) {
    return {

    };
}

export default connect(_mapStateToProps)(WaitHost);
