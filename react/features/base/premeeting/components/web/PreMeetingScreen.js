// @flow

import React, {PureComponent} from 'react';

import {translate} from '../../../../base/i18n';
import {connect} from '../../../../base/redux';
import {getAppToken} from '../../../../base/settings';
import Settings from '../../../../prejoin/components/Settings';
import {fetchMeeting} from '../../../../welcome/actions.web';
import {joinConference as joinConferenceAction, lock} from '../../../../prejoin/actions';
import {appNavigate} from '../../../../app/actions';
import Lobby from './Lobby';
import Logo from './Logo';
import WaitHost from './WaitHost';
import NoMeeting from './NoMeeting';

type Props = {
    children: React$Node;

    /**
     * Footer to be rendered for the page (if any).
     */
    footer?: React$Node;

    /**
     * The name of the participant.
     */
    name?: string;

    /**
     * Title of the screen.
     */
    title: string;

    /**
     * True if the preview overlay should be muted, false otherwise.
     */
    videoMuted?: boolean;

    /**
     * The video track to render as preview (if omitted, the default local track will be rendered).
     */
    videoTrack?: Object;
    meeting: Object;
    name: string;
    nameChange: Function;
    onLogin: Function;
    user: Object;
    onNext: Function;
    token: any;
    fetchMeeting: Function;
    onLobby: boolean;
    loginRequired: boolean;
    next: boolean;
    lock: Function;
    proceedToSettings: any;
    passwordChange: any;
    isUnLocked: boolean;
    wait: boolean;
};

/**
 * Implements a pre-meeting screen that can be used at various pre-meeting phases, for example
 * on the prejoin screen (pre-connection) or lobby (post-connection).
 */
class PreMeetingScreen extends PureComponent<Props> {
    /**
     * ComponentMount.
     *
     * @inheritdoc
     */
    componentDidMount() {
        const {meeting, user, onLobby, next} = this.props;
        let {token} = this.props;
        if ((token === null || token === '') && user !== null) {
            token = user.code;
        }
        const meetingId = location.href.split('?')[0].substring(location.href.lastIndexOf('/') + 1);

        if (meeting === null || parseInt(meeting.meetingId, 10) !== parseInt(meetingId, 10)) {
            this.props.lock();

            if (meetingId !== '') {
                this.props.fetchMeeting(meetingId, token, () => {});
            }
        }

        if (
            typeof meeting !== 'undefined' &&
            meeting !== null &&
            (meeting.isStarted === false || onLobby) &&
            !this.props.loginRequired &&
            next
        ) {
            setTimeout(() => {
                if (!this.props.loginRequired) {
                    if (
                        typeof this.props.user !== 'undefined' &&
                        this.props.user !== null &&
                        this.props.user.code !== null
                    ) {
                        token = this.props.user.code;
                    }
                    this.props.fetchMeeting(meeting.meetingId, token, () => {});
                }
            }, 10000);
        }
    }

    componentDidUpdate(oldProps) {
        const {meeting, user, onLobby, next} = this.props;
        let {token} = this.props;
        if ((token === null || token === '') && user !== null) {
            token = user.code;
        }
        if (
            (oldProps.onLobby === false &&
                onLobby === true &&
                meeting !== null &&
                typeof meeting !== 'undefined') ||
            (meeting !== null &&
                typeof meeting !== 'undefined' &&
                oldProps.next === false &&
                next === true)
        ) {
            if (meeting.meetingId) {
                this.props.fetchMeeting(meeting.meetingId, token, () => {});
            }
        }

        // ||
        // (typeof meeting !== 'undefined' &&
        // meeting !== null &&
        // oldProps.next === false &&
        // next === true)
    }

    /**
     * Implements {@code PureComponent#render}.
     *
     * @inheritdoc
     */
    render() {
        // const { title, videoMuted, videoTrack } = this.props;
        const {
            meeting,
            name,
            nameChange,
            onLogin,
            onNext,
            videoMuted,
            proceedToSettings,
            passwordChange,
            isUnLocked,
            wait
        } = this.props;

        // fxt-template-animation

        return (
            <div className='premeeting-screen' id='lobby-screen'>
                <section className='fxt-template-layout8 has-animation'>
                    <div className='fxt-content'>
                        <div className='fxt-header'>
                            <Logo />
                        </div>

                        <div className='fxt-form'>
                            {/* Pre-join */}
                            {meeting && meeting.error ? (
                                <NoMeeting />
                            ) : !isUnLocked &&
                              !wait &&
                              !this.props.onLobby &&
                              meeting !== null &&
                              meeting.meetingId ? (
                                <>
                                    <p className='text-center'>Join Meeting Room</p>

                                    <form onSubmit={proceedToSettings}>
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                <input
                                                    className='form-control'
                                                    disabled={true}
                                                    type='text'
                                                    value={`Meeting Room : ${meeting.meetingId}`}
                                                />
                                            </div>
                                        </div>
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                <input
                                                    className='form-control'
                                                    onChange={nameChange}
                                                    placeholder='Enter Your Name'
                                                    required='required'
                                                    type='text'
                                                    value={name}
                                                />
                                            </div>
                                        </div>
                                        {meeting.passwordProtected ? (
                                            <div className='form-group'>
                                                <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                    <input
                                                        className='form-control'
                                                        id='userid'
                                                        name='userid'
                                                        onChange={passwordChange}
                                                        placeholder='Enter Meeting Password'
                                                        type='password'
                                                    />
                                                </div>
                                            </div>
                                        ) : null}
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-4'>
                                                <button
                                                    className='fxt-btn-fill'
                                                    type='submit'
                                                    disabled={name == null || name == ''}>
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </>
                            ) : null}

                            {/* Settings */}
                            {isUnLocked && !wait && !this.props.onLobby ? (
                                <Settings onContinue={onNext} videoMuted={videoMuted} />
                            ) : null}
                            {/* Wait for host */}
                            {isUnLocked && wait ? <WaitHost onLogin={onLogin} /> : null}
                            {/* Wait on Lobby */}
                            {isUnLocked && this.props.onLobby && !wait ? <Lobby /> : null}
                        </div>
                    </div>
                </section>
            </div>
        );

        /*return (
            <div
                className='premeeting-screen'
                id='lobby-screen'>
                <section className='fxt-template-layout8 has-animation'>
                    <div className='fxt-content'>
                        <div className='fxt-header'>
                            <Logo />
                        </div>
                        <div className='fxt-form'>
                            { this.props.onLobby ? <Lobby /> : (meeting && meeting.meetingId) || meeting === null ? (
                                meeting === null || meeting.isStarted === false ? (
                                    <WaitHost onLogin={ onLogin } />
                                ) : isUnLocked ? (
                                    <Settings
                                        onContinue={ onNext }
                                        videoMuted={ videoMuted } />
                                ) : (
                                            <>
                                                <p className='text-center'>Join Meeting Room</p>
                                                <form onSubmit={ proceedToSettings }>
                                                    <div className='form-group'>
                                                        <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                            <input
                                                                className='form-control'
                                                                disabled={ true }
                                                                type='text'
                                                                value={ `Meeting Room : ${meeting.meetingId}` } />
                                                        </div>
                                                    </div>
                                                    <div className='form-group'>
                                                        <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                            <input
                                                                className='form-control'
                                                                onChange={ nameChange }
                                                                placeholder='Enter Your Name'
                                                                required='required'
                                                                type='text'
                                                                value={ name } />
                                                        </div>
                                                    </div>
                                                    { meeting.passwordProtected ? (
                                                        <div className='form-group'>
                                                            <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                                <input
                                                                    className='form-control'
                                                                    id='userid'
                                                                    name='userid'
                                                                    onChange={ passwordChange }
                                                                    placeholder='Enter Meeting Password'
                                                                    type='password' />
                                                            </div>
                                                        </div>
                                                    ) : null }
                                                    <div className='form-group'>
                                                        <div className='fxt-transformY-50 fxt-transition-delay-4'>
                                                            <button
                                                                className='fxt-btn-fill'
                                                                type='submit'>
                                                                Next
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </>
                                        )
                            ) : meeting && meeting.error ? (<NoMeeting />) : null
                            }
                        </div>
                    </div>
                </section>
            </div>
        );*/
    }
}

/**
 * Maps (parts of) the redux state to the React {@code Component} props.
 *
 * @param {Object} state - The redux state.
 * @returns {Object}
 */
function mapStateToProps(state): Object {
    const onLobby = state['features/prejoin'].onLobby;
    return {
        token: getAppToken(state),
        onLobby
    };
}

const mapDispatchToProps = {
    fetchMeeting,
    appNavigate,
    lock,
    joinConference: joinConferenceAction
};

export default connect(mapStateToProps, mapDispatchToProps)(translate(PreMeetingScreen));
