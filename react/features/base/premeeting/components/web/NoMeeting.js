// @flow

import React, { Component } from 'react';

import { connect } from '../../../redux';


export type Props = {

};

class NoMeeting extends Component<Props> {

    /**
     * Implements {@code Component#render}.
     *
     * @inheritdoc
     */
    render() {
        return (<div className='prejoin-error'>
            <p>
                <span>Unable to find a meeting with this request!</span>
                <a
                    className='ml-3'
                    href={ location.origin }>
                    Try Again
                </a>
            </p>
        </div>);
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @param {Props} ownProps - The own props of the component.
 * @returns {Props}
 */
function _mapStateToProps(state, ownProps) {
    return {

    };
}

export default connect(_mapStateToProps)(NoMeeting);
