// @flow

import React, { Component } from 'react';

import { connect } from '../../../redux';


export type Props = {

};

class Logo extends Component<Props> {

    /**
     * Implements {@code Component#render}.
     *
     * @inheritdoc
     */
    render() {
        return (<a
            className='fxt-logo'
            disabled={ true }
            href='#'
            onClick={ (e) => { e.preventDefault(); } }>
            <img
                alt='rounddesk'
                src='./images/logo-rounddesk.png' />
        </a>);
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @param {Props} ownProps - The own props of the component.
 * @returns {Props}
 */
function _mapStateToProps(state, ownProps) {
    return {

    };
}

export default connect(_mapStateToProps)(Logo);
