// @flow

import React, { Component } from 'react';

import { connect } from '../../../redux';


export type Props = {

};

class Lobby extends Component<Props> {

    /**
     * Implements {@code Component#render}.
     *
     * @inheritdoc
     */
    render() {
        return (<><span className='lobby-text text-center'>Waiting on lobby...</span><div className='spinner'>Loading...</div></>);
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @param {Props} ownProps - The own props of the component.
 * @returns {Props}
 */
function _mapStateToProps(state, ownProps) {
    return {

    };
}

export default connect(_mapStateToProps)(Lobby);
