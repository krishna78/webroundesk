// @flow

import React, { Fragment } from 'react';

import { BaseApp } from '../../base/app';
import { toURLString } from '../../base/util';
import { OverlayContainer } from '../../overlay';
import { fetchMeeting } from '../../welcome/actions';
import { appNavigate } from '../actions';
import { getDefaultURL } from '../functions';


/**
 * The type of React {@code Component} props of {@link AbstractApp}.
 */
export type Props = {

    /**
     * XXX Refer to the implementation of loadURLObject: in
     * ios/sdk/src/JitsiMeetView.m for further information.
     */
    timestamp: any,

    /**
     * The URL, if any, with which the app was launched.
     */
    url: Object | string;
};

/**
 * Base (abstract) class for main App component.
 *
 * @abstract
 */
export class AbstractApp extends BaseApp<Props, *> {
    _init: Promise<*>;

    /**
     * Initializes the app.
     *
     * @inheritdoc
     */
    componentDidMount() {
        super.componentDidMount();


        this._init.then(() => {


            // If a URL was explicitly specified to this React Component, then
            // open it; otherwise, use a default.
            // this._openURL(toURLString(this.props.url) || this._getDefaultURL());

            if (toURLString(this.props.url) && toURLString(this.props.url) !== '/') {

                this._openURL(toURLString(this.props.url));
            } else {

                this.state.store.dispatch(appNavigate(toURLString(this._getDefaultURL())));
            }
        });
    }

    /**
     * Implements React Component's componentDidUpdate.
     *
     * @inheritdoc
     */
    componentDidUpdate(prevProps: Props) {


        const previousUrl = toURLString(prevProps.url);

        const currentUrl = toURLString(this.props.url);

        const previousTimestamp = prevProps.timestamp;
        const currentTimestamp = this.props.timestamp;

        this._init.then(() => {
            // Deal with URL changes.


            if (previousUrl !== currentUrl

                // XXX Refer to the implementation of loadURLObject: in
                // ios/sdk/src/JitsiMeetView.m for further information.
                || previousTimestamp !== currentTimestamp) {

                if (currentUrl) {

                    this._openURL(currentUrl);
                } else {

                    this.state.store.dispatch(appNavigate(toURLString(this._getDefaultURL())));
                }
            }
        });
    }

    /**
     * Creates an extra {@link ReactElement}s to be added (unconditionaly)
     * alongside the main element.
     *
     * @abstract
     * @protected
     * @returns {ReactElement}
     */
    _createExtraElement() {
        return (
            <Fragment>
                <OverlayContainer />
            </Fragment>
        );
    }

    /**
     *
     */
    _createMainElement: (React$Element<*>, Object) => ?React$Element<*>;

/**
 * Gets the default URL to be opened when this {@code App} mounts.
 *
 * @protected
 * @returns {string} The default URL to be opened when this {@code App}
 * mounts.
 */
_getDefaultURL() {
    return getDefaultURL(this.state.store);
}

/**
 * Navigates this {@code AbstractApp} to (i.e. Opens) a specific URL.
 *
 * @param {Object|string} url - The URL to navigate this {@code AbstractApp}
 * to (i.e. The URL to open).
 * @protected
 * @returns {void}
 */
_openURL(url) {

    const link = new URL(url);

    // this.state.store.dispatch(appNavigate(toURLString(url)));
    const paths = link.pathname.split('/').filter(Boolean);

    if (paths.length === 2) {

        // const roomId = paths[0];
        const meetingId = paths[1];
        const token = this.getParameterByName('join', link.search);
        if (navigator.product !== 'ReactNative') {

            this.state.store.dispatch(appNavigate(toURLString(url)));
        } else {

            this.state.store.dispatch(fetchMeeting(meetingId, token !== null ? token : '', () => { }));
        }
    } else {

        if (link.host === 'meet.roundesk.io' && navigator.product === 'ReactNative') {

            this.state.store.dispatch(appNavigate(toURLString(url)));
        } else {

            this.state.store.dispatch(appNavigate(toURLString(this._getDefaultURL())));
        }
    }
}

getParameterByName(name, search) {
    const match = RegExp('[?&]' + name + '=([^&]*)').exec(search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
}
